#!/bin/bash

## logging utility for shell script
# It supports log filter and coloring based on supplied level.

# shellcheck disable=SC2034
# LOG_LEVEL_TRACE is reserved
LOG_LEVEL_TRACE=10
LOG_LEVEL_DEBUG=20
LOG_LEVEL_INFO=30
LOG_LEVEL_WARN=40
LOG_LEVEL_ERROR=50
# ANSI text color: use 9x instead of 3x for high-contrast.
# shellcheck disable=SC2034
# LOG_COLOR_TRACE is reserved
LOG_COLOR_TRACE='\e[90m' # black
LOG_COLOR_DEBUG='\e[94m' # light blue
LOG_COLOR_INFO='\e[97m'  # white
LOG_COLOR_WARN='\e[95m'  # purple
LOG_COLOR_ERROR='\e[91m' # red
LOG_COLOR_DATE='\e[96m'  # cyan (deep blue)
LOG_COLOR_PROG='\e[92m'  # green

function stack_trace() {
    ## printing the calling stack
    #* ${BASH_SOURCE[$i+1]}(${BASH_LINENO[$i]}) -> ${FUNCNAME[$i]}
    #* ${FUNCNAME[$i]} (${BASH_LINENO[$i-1]}) -> ${FUNCNAME[$i-1]}
    #* where ${FUNCNAME[$i]} is defined in ${BASH_SOURCE[$i]}.
    #* edge cases:
    #* - ${BASH_SOURCE[0]} is the script of the current execution line, no
    #*   shell function is called from there. Referring the current execution
    #*   line by $LINENO. In interactive shell, this variable is empty.
    #* - ${FUNCNAME[-1]} is fixed to `main` if accessed from shell function.
    #*   Empty if accessed from script, and invalid from interactive shell.
    #*   ${BASH_LINENO[-1]} is fixed to `0` if accessed from shell function
    #*   or script, and invalid from interactive shell.
    local i
    for ((i = 0; i < ${#BASH_SOURCE[@]}; ++i)); do
        if [ "$i" -eq 0 ]; then
            echo "${i}: ${BASH_SOURCE[$i]}: ${LINENO}: ${BASH_COMMAND}"
        else
            echo "${i}: ${BASH_SOURCE[$i]}: ${BASH_LINENO[$i - 1]}: ${FUNCNAME[$i - 1]} (defined in ${BASH_SOURCE[$i - 1]})"
        fi
    done 1>&2
    echo "x: x: ${BASH_LINENO[-1]}: ${FUNCNAME[-1]}" 1>&2
}

function get_log_level(){
    local _log_level=$1
    case $_log_level in
    10 | 20 | 30 | 40 | 50) ;;
    error | ERROR) _log_level=$LOG_LEVEL_ERROR ;;
    warn | WARN | warning | WARNING) _log_level=$LOG_LEVEL_WARN ;;
    info | INFO) _log_level=$LOG_LEVEL_INFO ;;
    debug | DEBUG) _log_level=$LOG_LEVEL_DEBUG ;;
    trace | TRACE | tracing | TRACING) _log_level=$LOG_LEVEL_TRACE ;;
    *)
        echo "error: invalid LOG_LEVEL value '${_log_level}'." 1>&2
        return 1
        ;;
    esac
    echo "$_log_level"
}

function log() {
    #> LOG_LEVEL-related environment variables: LOG_LEVEL, log_level, LOGLEVEL, loglevel
    #> setting anyone of them should be equivalent.
    #> They can be set as a number (10~50) or labels (trace, debug, info, warn, error).
    #> Labels can be set either in lowercase or uppercase.
    local log_level_limit=${LOG_LEVEL:-${log_level:-${LOGLEVEL:-${loglevel:-$LOG_LEVEL_INFO}}}}
    log_level_limit=$(get_log_level "$log_level_limit")

    local color_prefix=${LOG_COLOR_INFO}
    local color_suffix='\e[0m'
    
    declare -i num_level
    local the_level
    the_level=$(tr '[:upper:]' '[:lower:]' <<<"$1")
    case $the_level in
    error)
        num_level=${LOG_LEVEL_ERROR}
        color_prefix=${LOG_COLOR_ERROR}
        shift
        ;;
    warn | warning)
        the_level='warning'
        num_level=${LOG_LEVEL_WARN}
        color_prefix=${LOG_COLOR_WARN}
        shift
        ;;
    debug)
        num_level=${LOG_LEVEL_DEBUG}
        color_prefix=${LOG_COLOR_DEBUG}
        shift
        ;;
    info | *)
        num_level=${LOG_LEVEL_INFO}
        color_prefix=${LOG_COLOR_INFO}
        if [ "$the_level" = 'info' ]; then
            shift
        else
            the_level='info'
        fi
        ;;
    esac

    local opt_prefix opt_verbose end_options
    while [ -n "$1" ]; do
        case $1 in
        --prefix) opt_prefix=${2:?"prefix not provided!"} && shift ;;
        --verbose) opt_verbose=true ;;
        --) shift && break ;; # break the scan loop to keep other arguments
        *) break ;;           # break the scan loop to keep other arguments
        esac
        shift
    done

    if [ "$opt_verbose" = true ]; then
        stack_trace
    fi

    if [[ "$num_level" -ge "$log_level_limit" ]]; then
        # If no message provided for logging, ignore the invoking.
        local PROGRAM
        PROGRAM=$(basename "${BASH_SOURCE[1]}")
        local LN=${BASH_LINENO[0]}
        declare -a msg_list
        local i message tf_start=false
        for ((i = 1; i <= "$#"; ++i)); do
            if [ -z "${!i}" ]; then continue; fi
            readarray -t msg_list <<<"${!i}"
            if [ "$tf_start" = false ]; then
                echo -e "${LOG_COLOR_DATE}[$(date +'%F %T')] ${LOG_COLOR_PROG}$PROGRAM($LN) ${color_prefix}[$the_level]: ${msg_list[0]}${color_suffix}"
                msg_list=("${msg_list[@]:1}")
                tf_start=true
            fi
            for message in "${msg_list[@]}"; do
                # if msg_list is empty (length=0), loop will not be entered
                echo -e "${color_prefix}${opt_prefix}${message}${color_suffix}"
            done
        done 1>&2
    fi
}

function assert_empty() {
    ## assert if a variable is empty, and log the error info.
    var_name=${1:?"variable name not specified as argument 1."}
    eval "var=\${$var_name}"
    if [ -z "$var" ]; then
        if [ -n "$2" ]; then
            log error "$2"
        else
            log error "$var_name is not specified!"
        fi
        return 1
    fi
}
