#!/bin/bash

function load() {
    # local _SCRIPTDIR_ to avoid pollute caller environment.
    local _SCRIPTDIR_
    _SCRIPTDIR_=$(dirname "$(realpath "${BASH_SOURCE[0]}")")
    if ! command -v log >/dev/null; then
        source "${_SCRIPTDIR_}/logging.sh"
    fi
}
load

function submodule_init() {
    if [[ "$1" = '-h' || $1 = '--help' ]]; then
        echo "workflow: git submodule init."
        echo "  pull the update of the parent project's latest refer of submodules, recursively."
        echo ""
        echo "syntax: submodule_init [submodule-path]."
        echo ""
        echo "note: run this command under a git repository."
        return 1
    fi
    local submodule_path=${1}
    if [ -z "$submodule_path" ]; then
        log warning "submodule path is not specified, will update all submodules."
    fi

    #* using `--merge` to prevent checkout to a detached branch.
    #> If you mean to perform some experiments on the submodule without requirement
    #> to save the changes, the you can call
    #+     git submodule update <submodule_path>
    #> on that submodule.
    # 
    # shellcheck disable=SC2086
    # `submodule_path` can be empty, which implies updating all submodules.
    git submodule update --init --merge --recursive $submodule_path
}
alias git_submodule_init=submodule_init

function submodule_update() {
    if [[ "$1" = '-h' || $1 = '--help' ]]; then
        echo "workflow: git submodule update to parent project's referred commit."
        echo "  pull latest referred commit of submodules in the parent project, recursively."
        echo ""
        echo "syntax: submodule_update [submodule-path]."
        echo ""
        echo "note: run this command under a git repository."
        echo "      If you want to track the latest update of a submodule from the upstream,"
        echo "      please run \`submodule_pull_upstream [submodule-path]\`."
        return 1
    fi
    local submodule_path=${1}
    if [ -z "$submodule_path" ]; then
        log warning "submodule path is not specified, will update all submodules."
    fi

    ### update submodules according to parent's commit record
    #> the remote branch name may be overridden by setting the `submodule.<name>.branch`
    #> option in either `.gitmodules` or `.git/config` (with .git/config taking precedence).
    #* always using option `--merge` to merge changes from upstream to current HEAD 
    #* of the submodule.
    #* `--init`: Initialize all submodules for which "git submodule init" has 
    #* not been called so far before updating.
    #
    # shellcheck disable=SC2086
    # `submodule_path` can be empty, which implies updating all submodules.
    git submodule update --init --merge --recursive $submodule_path
}
alias git_submodule_update=submodule_update

function submodule_pull_upstream(){
    if [[ "$1" = '-h' || $1 = '--help' ]]; then
        echo "workflow: git submodule update to upstream."
        echo "  pull latest update of submodules from remote repository, recursively."
        echo ""
        echo "syntax: submodule_pull_upstream [submodule-path]."
        echo ""
        echo "note: run this command under a git repository."
        echo "      If you only want the submodule being updated with parent project,"
        echo "      please run \`submodule_update [submodule-path]\`."
        echo "      After this update operation, you may also need to update parent"
        echo "      project's referred commit of the submodule."
        return 1
    fi
    local submodule_path=${1}
    if [ -z "$submodule_path" ]; then
        log warning "submodule path is not specified, will update all submodules."
    fi

    ### update via submodule's remote tracking branch
    #* enable this by option `--remote`.
    #> the remote branch name may be overridden by setting the `submodule.<name>.branch`
    #> option in either `.gitmodules` or `.git/config` (with .git/config taking precedence).
    #> Use this option to integrate changes from the upstream sub-project with your 
    #> submodule's current HEAD (equivalent to run git pull from submodule).
    #* always using option `--merge` to merge changes from upstream to current HEAD 
    #* of the submodule.
    #* `--init`: Initialize all submodules for which "git submodule init" has 
    #* not been called so far before updating.
    #
    # shellcheck disable=SC2086
    # `submodule_path` can be empty, which implies updating all submodules.
    git submodule update --init --remote --merge --recursive
}
alias git_submodule_pull_upstream=submodule_pull_upstream

function cherry_pick() {
    if [[ "$1" = '-h' || $1 = '--help' ]]; then
        echo "usage: git cherry-pick workflow."
        echo "  copy selected commits from other branches to the current branch."
        echo ""
        echo "syntax: cherry_pick commit_id..."
        echo ""
        echo "note: run this command under a git repository."
        return 1
    fi
    log error "cherry_pick not implemented!"
    return 1
}
