#!/usr/bin/env -S bats --timing --show-output-of-passing-tests --verbose-run

# execute this scripts from "SCRIPTDIR" to run all test cases.
# see "bats --help" for invoking individual test cases.

function setup() {
    load ../shell.sh
}

@test "subset is true" {
    a1=(x y z)
    a2=(x y z s)
    subset_of_array a1 a2
}

@test "subset is false" {
    a1=(x y z)
    a2=(x y w)
    bats_require_minimum_version 1.5.0
    run -1 subset_of_array a1 a2
}

@test "array find is true" {
    a1=(x y z)
    x=y
    [ $(arrayfind a1 "$x") -eq 1 ]
}

@test "array find is false" {
    a1=(x y z)
    x=a
    i=0
    [ $(arrayfind a1 "$x") -eq -1 ]
    echo "i=$i"
}

#  local iterator variable in ((...)) will not propagate to invoker's scope
@test "array find local iterator index" {
    a1=(x y z)
    x=a
    i=999
    arrayfind a1 "$x" >/dev/null
    echo "i=$i"
    [ $i -eq 999 ]
}


@test "array find local iterator variable" {
    a1=(x y z)
    a2=(x y z s)
    x=999
    subset_of_array a1 a2 >/dev/null
    [ $x -eq 999 ]
}
