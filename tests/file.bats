#!/usr/bin/env -S bats --timing --show-output-of-passing-tests --verbose-run

function setup() {
    load ../file.sh
}

@test "clean cache files dry run" {
    export LOG_LEVEL=DEBUG
    clean_outdated_files --dry-run "$(dirname "$BATS_TEST_FILENAME")"
}
