#!/bin/bash -e

function on_error(){
    # shellcheck disable=SC2034
    ERROR_CODE=$?
    echo "error code: $?" 1>&2
}

function on_stop() {
    echo "stop signal: "
}

function on_exit() {
    echo "Program exit with code $?" 1>&2
}

function on_interrupt(){
    # most often used, use it to perform clean on certain operation.
    echo "" 1>&2
    echo "Interrupt from keyboard (Ctrl+C), receive SIGINT(2)" 1>&2
    exit 2
}

function on_terminate() {
    echo "Program terminated with SIGTERM(15)" 1>&2
    exit 15
}

function on_quit(){
    echo "Program quit with SIGQUIT(3)" 1>&2
    exit 3
}

trap on_error ERR
trap on_exit EXIT
trap on_interrupt SIGINT
trap on_terminate SIGTERM
trap on_quit SIGQUIT

# SIGKILL/SIGSTOP can not be trapped.
# SIGSTOP will be triggered by Ctrl+Z, program is stopped.
# you can use jobs to see this program, and use
#+ bg/fg JOB_ID
# to move the job to background/foreground to restore
# execute.
# Ctrl+D will exit the current shell, if no foreground job.
while true; do
    date
    sleep 1
done
