#!/usr/bin/env -S bats --timing --show-output-of-passing-tests --verbose-run

# execute this scripts from "SCRIPTDIR" to run all test cases.
# see "bats --help" for invoking individual test cases.

setup() {
    load ../logging.sh
    load ../installer.sh
    export LOG_LEVEL=DEBUG
}

@test "get mapped container arch" {
    test "$(_get_mapped_container_arch)" = $(sed 's/x86_64/amd64/;s/aarch64/arm64/' <<<"$(arch)")
}

@test "get mapped nodejs arch" {
    mapped_arch=$(_get_mapped_app_arch nodejs)
    log debug "nodejs arch: ${mapped_arch}"
    test "$mapped_arch" = $(sed 's/x86_64/x64/;s/aarch64/arm64/' <<<"$(arch)")
}
