#!/usr/bin/env -S bats --timing --show-output-of-passing-tests --verbose-run

function setup() {
    function test_local_scope_variable_refer(){
        local var_a=100
        function internal_function(){
            # if modified by local, the refer to parent variable
            echo "internal_function: try to refer local variable of parent function: var_a=$var_a"
            local var_a=1
            echo "internal_function: try to refer local variable of current function: var_a=$var_a"
        }
        internal_function
        echo "parent_function: try to refer local variable of current function: var_a=$var_a"
    }
}

@test "local variable referring" {
    # local variables in parent function can be referred by child function
    # child function define the same name variable will 
    test_local_scope_variable_refer
}
