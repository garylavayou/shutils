#!/usr/bin/env -S bats --timing --show-output-of-passing-tests --verbose-run
# use bats for bash testing

# shellcheck source-path=SCRIPTDIR/../
# echo "external-sources=true" > ~/.shellcheckrc
function setup() {
    load ../shell.sh
    function eval_command(){
        val=$1
        for c in "${EVAL_CMD[@]}"; do
            set +e
            eval "$c"
            x0=$?
            if [ "$val" = false ]; then
                test $x0 -ne 0
                x=$?
            else
                x=$x0
            fi
            set -e
            if [ "$x" -ne 0 ]; then
                echo "error: '${c}' return $x0.".
            else
                echo "info: '${c}' return $x0."
            fi
        done
    }
    #! avoid function name collision with test case name
    #* see: <https://github.com/bats-core/bats-core/issues/923>.
    function vcmp_invalid_op() {
        vcmp 2.0.0 -lx 1.0.0
    }
}



@test "vcmp gt" {
    vcmp 1.0.0 -gt 0.0.0
    vcmp 1.2.0 -gt 1.1.0
    vcmp 1.2.2 -gt 1.2.1
}

@test "vcmp gt empty version" {
    # empty version string must be ignored, specified as empty string ''
    # otherwise, vcmp will not pass argument checking.
    vcmp 1.2.1 -gt ''
}

@test "vcmp gt false" {
    EVAL_CMD=(
        'vcmp 1.0.0 -gt 2.0.0'
        'vcmp 1.2.0 -gt 1.3.0'
        'vcmp 1.2.2 -gt 1.2.4'
    )
    eval_command false
}

@test "vcmp gt empty version false" {
    # empty version string must be ignored, specified as empty string ''
    # otherwise, vcmp will not pass argument checking.
    bats_require_minimum_version 1.5.0
    run -1 vcmp '' -gt 1.2.3
}

@test "vcmp ge" {
    vcmp 1.0.0 -ge 0.0.0
    vcmp 1.0.0 -ge 1.0.0
    vcmp 1.2.0 -ge 1.1.0
    vcmp 1.2.0 -ge 1.2.0
    vcmp 1.2.2 -ge 1.2.1
    vcmp 1.2.2 -ge 1.2.2
}

@test "vcmp eq" {
    vcmp 1.0.0 -eq 1.0.0
    vcmp 0.1.0 -eq 0.1.0
    vcmp 0.0.1 -eq 0.0.1
    vcmp 1.2.0 -eq 1.2.0
}

@test "vcmp eq both empty version" {
    # empty version string must be ignored, specified as empty string ''
    # otherwise, vcmp will not pass argument checking.
    vcmp '' -eq ''
}

@test "vcmp eq single empty version false" {
    # empty version string must be ignored, specified as empty string ''
    # otherwise, vcmp will not pass argument checking.
    bats_require_minimum_version 1.5.0
    run -1 vcmp 1.2.1 -eq ''
}

@test "vcmp le" {
    vcmp 0.0.0 -le 1.0.0
    vcmp 1.0.0 -le 1.0.0
    vcmp 1.1.0 -le 1.2.0
    vcmp 1.2.0 -le 1.2.0
    vcmp 1.2.0 -le 1.2.1
    vcmp 1.2.1 -le 1.2.1
}

@test "vcmp le false" {
    EVAL_CMD=(
        'vcmp 2.0.0 -le 1.0.0'
        'vcmp 2.1.0 -le 2.0.0'
        'vcmp 3.2.2 -le 3.2.1'
    )
    eval_command false
}

@test "vcmp lt" {
    vcmp 0.0.0 -lt 1.0.0
    vcmp 1.1.0 -lt 1.2.0
    vcmp 1.2.0 -lt 1.2.1
}

@test "vcmp invalid op" {
    bats_require_minimum_version 1.5.0
    run -2 vcmp_invalid_op
    if [ "$status" -ne 0 ]; then
        echo "invalid operator detected successfully (exit status: $status)."
    else
        echo "invalid operator not detected."
        return 1
    fi
}
