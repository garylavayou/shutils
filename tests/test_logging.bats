#!/usr/bin/env -S bats --timing --show-output-of-passing-tests
# use bats for bash testing

# shellcheck source-path=SCRIPTDIR/../
# echo "external-sources=true" > ~/.shellcheckrc

function setup() {
    load ../logging.sh
}

@test "logging info" {
    log info "this is a normal log."
}

@test "logging info with prefix" {
    log info --prefix '\t' \
         "this is a normal log." \
         "details about this log (with tab indentation)."
}

@test "logging warning" {
    log warn "this is a warn log."
}

@test "logging warning with prefix" {
    traces=$(stack_trace)
    log warn --prefix '  |' \
        "this is a long warning log (stack traces):" "${traces}"
}

@test "logging error" {
    log error "this is an error log."
}

@test "logging debug" {
    export LOG_LEVEL=$LOG_LEVEL_DEBUG
    log debug "this is a debug log."
}

@test "logging all" {
    export LOG_LEVEL=$LOG_LEVEL_TRACE
    log debug "this is a debug log."
    log info "this is a normal log."
    log warning "this is a warning log."
    log error "this is a error log."
    # TODO use assert_output to verify all is logged out
}

@test "logging filter" {
    export LOG_LEVEL=$LOG_LEVEL_WARN
    log debug "this is a debug log."
    log info "this is a normal log."
    log warning "this is a warning log."
    log error "this is a error log."
    # TODO use assert_output to verify filter is applied
}

# bats test_tags=level:string,level:debug
@test "logging debug with string log level debug" {
    export LOG_LEVEL=DEBUG
    run log debug "this is a debug log."
    grep 'debug' <<<"$output" >/dev/null
}

# bats test_tags=level:string,level:info
@test "logging debug with string log level info" {
    export LOG_LEVEL=INFO
    run log debug "this is a debug log."
    ! grep 'debug' <<<"$output" >/dev/null
}

# bats test_tags=level:altname,level:debug
@test "logging debug with alternative loglevel variable" {
    export loglevel=DEBUG
    run log debug "this is a debug log."
    grep 'debug' <<<"$output" >/dev/null
}

# bats test_tags=level:altname,level:debug
@test "logging debug with alternative log_level variable" {
    export log_level=DEBUG
    run log debug "this is a debug log."
    grep 'debug' <<<"$output" >/dev/null
}