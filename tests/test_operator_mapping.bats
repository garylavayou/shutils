#!/usr/bin/env -S bats --timing --show-output-of-passing-tests --verbose-run
# use bats for bash testing

# shellcheck source-path=SCRIPTDIR/../
# echo "external-sources=true" > ~/.shellcheckrc

function setup() {
    load ../shell.sh
}

@test "map operator consistence" {
    for op in '=' '==' '!=' '~=' '>=' '<=' '>' '<'; do 
        op1=$(map_operator "$op")
        op2=${operators["$op"]}
        if [ "$op1" = "$op2" ]; then
            echo "$op1 <-> $op2"
        else
            echo "error: $op1 x $op2."
            return 1
        fi
    done
}
