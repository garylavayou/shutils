#!/bin/bash

function load() {
    # local _SCRIPTDIR_ to avoid pollute caller environment.
    local _SCRIPTDIR_
    _SCRIPTDIR_=$(dirname "${BASH_SOURCE[0]}")
    if ! command -v log >/dev/null; then
        source "${_SCRIPTDIR_}/logging.sh"
    fi
}
load

function show_cert() {
    local certfile
    if [ -z "$1" ]; then
        log error "usage: show_cert cert_domain"
        return 1
    elif [[ ! "$1" =~ .crt$ ]]; then
        certfile="$1.crt"
    else
        certfile="$1"
    fi
    openssl x509 -text -noout -in "$certfile"
}

function log_cert_info() {
    local cert_name=${1:?"please specify cert name!"}
    local cert_info
    cert_info=$(show_cert "${cert_name}")
    log debug --prefix '>>' "${cert_name} certificate information:" "${cert_info}"
}

function create_root_ca_certs() {
    if [[ -z "$1" || "$1" =~ -h|--help|help ]]; then
        log info --prefix '| ' \
            "usage: create_root_ca_certs CA_NAME" \
            "CA_NAME should correspond to the configuration file name."
        return 1
    fi
    local cert_name=${1:-'rootca'}
    if [[ -e "${cert_name}.crt" && -e "${cert_name}.key" ]]; then
        log warning --prefix ' ! ' \
            "root CA certificate and credential exist, skip create!" \
            "If you want to re-create it, please first manually remove the credential files."
        return 1
    fi
    if [ ! -e "${cert_name}.conf" ]; then
        log error --prefix ' |' \
            "no [${cert_name}.conf] in current working directory."
        log info --prefix ' |' "" \
            "You may want to copy the CA certificate from other place." \
            "If you really need to create a new CA certificate, please create" \
            "[${cert_name}.conf] from template file:" \
            ">>: cp $(pwd)/rootca.conf.template $(pwd)/rootca.conf"
        return 1
    fi
    declare -a key_opts=()
    if [ -e "${cert_name}.key" ]; then
        key_opts+=(-key "${cert_name}.key")
    else
        key_opts+=(-newkey 'rsa:2048' -keyout "${cert_name}.key")
    fi
    set -x
    openssl req -x509 \
        -sha256 -days 36500 \
        -nodes \
        -config "${cert_name}.conf" \
        "${key_opts[@]}" \
        -out "${cert_name}.crt"
    { set +x; } 2>/dev/null
    # set CA's certificate and private key to readonly to avoid accidentally overwrite it.
    chmod 444 "${cert_name}.crt" # rw-r--r-- -> r--r--r--
    chmod 400 "${cert_name}.key" # rw------- -> r--------
    log_cert_info "$cert_name"
}

## Verify if cert and key match
function check_key_cert_match() {
    if [[ -z "$1" || "$1" =~ -h|--help|help ]]; then
        log info --prefix '| ' \
            "usage: check_key_cert_match CERT_NAME" \
            "CERT_NAME should correspond to the certification file name."
        return 1
    fi

    local cert_name
    cert_name=$(basename --suffix .crt "$1")
    x1=$(openssl rsa -noout -modulus -in "${cert_name}.key" | openssl md5)
    x2=$(openssl x509 -noout -modulus -in "${cert_name}.crt" | openssl md5)
    if [ "$x1" = "$x2" ]; then
        return 0
    else
        return 1
    fi
}

function convert_cert_bundle() {
    # useful for create client certificate bundle
    if [[ -z "$1" || "$1" =~ -h|--help|help ]]; then
        log info --prefix '| ' \
            "usage: convert_cert_bundle CERT_NAME" \
            "CERT_NAME should correspond to the certification file name."
        return 1
    fi

    local cert_name
    cert_name=$(basename --suffix .crt "$1")
    set -x
    cat "${cert_name}.key" "${cert_name}.crt" >"${cert_name}-bundle.pem"
    { set +x; } 2>/dev/null
}

function convert_cert_to_pkcs12() {
    # useful for create client certificate bundle in PKCS#12 form.
    if [[ -z "$1" || "$1" =~ -h|--help|help ]]; then
        log info --prefix '| ' \
            "usage: convert_cert_to_pkcs12 CERT_NAME" \
            "CERT_NAME should correspond to the certification file name."
        return 1
    fi

    local cert_name
    cert_name=$(basename --suffix .crt "$1")
    set -x
    openssl pkcs12 -export \
        -out "${cert_name}.pfx" \
        -inkey "${cert_name}.key" -in "${cert_name}.crt"
    { set +x; } 2>/dev/null
}

function create_private_key() {
    set -x
    openssl genrsa -out "${1:-server}.key" 2048
    { set +x; } 2>/dev/null
}

function create_cert_sign_request() {
    # $2 can be ignored if the csr.conf is named as $1 (server name)
    local server_name=${1:-server} && shift
    local conf_name=${1:-$server_name} # $1 is empty => $2 also empty
    if [[ ! "$conf_name" =~ \.csr\.conf$ ]]; then
        conf_name="${conf_name}.csr.conf"
    fi
    if [ ! -e "$conf_name" ]; then
        conf_name=csr.conf
    fi
    set -x
    openssl req -new -key "${server_name}.key" -out "${server_name}.csr" -config "${conf_name}"
    { set +x; } 2>/dev/null
}

function create_signed_cert() {
    # the server should send <server>.csr and <server>.conf to the CA to create
    # a signed certificated for it.
    if [ $# -lt 2 ]; then
        log error --prefix " |" "argument missing" \
            "please specify CA and server name for configuration files." \
            "e.g., create_signed_cert rootca server"
        return 1
    fi
    local ca_name="${1:-rootca}"
    local cert_name="${2:-server}"
    set -x
    openssl x509 -req \
        -CAcreateserial -CA "${ca_name}.crt" -CAkey "${ca_name}.key" \
        -days 3650 \
        -sha256 \
        -in "${cert_name}.csr" -extfile "${cert_name}.conf" \
        -out "${cert_name}.crt"
    { set +x; } 2>/dev/null
    log_cert_info "${cert_name}"
}

function check_csr_conf() {
    local csr_conf=${1:-csr.conf}
    if [ ! -e "${csr_conf}" ]; then
        log error "error: ${csr_conf} does not exist, cannot sign a certificate for the server."
        log info --prefix ' |' '' \
            "The server may obtain an exist certificate and the associated server.csr.conf" \
            "from a provider." \
            ">>: cp path/to/server.{crt,key,.csr.conf} path/to/certs/" \
            "If you really need to continue the process, please copy csr.conf.template and" \
            "modify the configurations." \
            ">>: cp path/to/certs/csr.conf.template path/to/certs/server.csr.conf"
        return 1
    fi
}

function check_server_conf() {
    local server_conf=${1:-server.conf}
    if [ ! -e "${server_conf}" ]; then
        log error --prefix ' | ' \
            "${server_conf} does not exist" \
            "cannot create a certificate sign request for the server."
        log info --prefix ' | ' "" \
            "The server may obtain an exist certificate and the associated server.conf from" \
            "a provider." \
            ">>: cp path/to/server.{crt,key,conf} path/to/certs/" \
            "If you really need to continue the process, please copy server.conf.template" \
            "and modify the configurations." \
            ">>: cp path/to/certs/server.conf.template path/to/certs/server.conf"
        return 1
    fi
}

function get_certs_update_status() {
    # output value:
    # 0   - certificated is up-to-date, will not be changed.
    # 1   - the server certificate will change
    # 2,3 - the root CA and server certificates will change.
    local cert_path ca_name server_name
    while [ -n "$1" ]; do
        case "$1" in
        --cert-path) cert_path=${2} && shift ;;
        --ca-name) ca_name=${2} && shift ;;
        esac
        shift
    done
    server_name=${1:-'server'}
    cert_path=${cert_path:-'.'}
    ca_name=${ca_name:-'rootca'}
    if [ ! -e "$cert_path" ]; then
        echo 3
    elif [[ ! (-e ${cert_path}/${ca_name}.key && -e ${cert_path}/${ca_name}.crt) ]]; then
        echo 2
    elif [[ ! (-e ${cert_path}/${server_name}.key && -e ${cert_path}/${server_name}.crt) ]]; then
        echo 1
    else
        echo 0
    fi
}

function create_certificate_chain() {
    if [[ -z "$1" || "$1" =~ (--)?help ]]; then
        echo "usage:   CA_NAME=ca_name create_certificate_chain server_name"
        echo "example: CA_NAME=rootCA create_certificate_chain garyhome.net"
        return 1
    fi

    local CA_NAME=${CA_NAME:-'rootca'}
    local server=${1:-'server'}

    # Create cert file via ${CA_NAME} for server from server's key file
    if [[ ! -e "${server}.key" ]]; then
        log info "${server} credentials does not exist, create private key ......"
        create_private_key "$server"
    else
        log info --prefix '>>:' "${server} credentials present:" \
            "$(ls -lh "${server}.key")"
    fi
    if [[ ! -e "${server}.crt" ]]; then
        # Create Root-CA certs only when necessary
        if [[ -e "${CA_NAME}.key" && -e "${CA_NAME}.crt" ]]; then
            log info --prefix '>>:' "Root CA credentials present:" \
                "$(ls -lh "${CA_NAME}.key" "${CA_NAME}.crt")"
            log_cert_info "${CA_NAME}"
        elif [[ -e "${CA_NAME}.key" && -e "${CA_NAME}.crt" ]]; then
            log error "Root CA certificate or credential is missing, please check local files."
            return 1
        else
            log warning "Root CA certificate and credential are missing, create it automatically."
            create_root_ca_certs "${CA_NAME}"
        fi
        log info "creating signed certificate for ${server} on behalf of Root CA ......"
        check_csr_conf "${server}.csr.conf"
        check_server_conf "${server}.conf"
        create_cert_sign_request "${server}"
        create_signed_cert "${CA_NAME}" "${server}"
    else
        log info --prefix '>>:' "signed certificate for ${server} present:" \
            "$(ls -lh "${server}.crt")"
        log_cert_info "${server}"
        # if root-ca is refreshed the server certs must be recreated
        # TODO: check existing server cert's issuer is consistent with the local root-ca
    fi
}

function install_certificates() {
    ## install CA certificate files into system's trust store.
    # usage:
    #> install_certificates file_or_dir
    # where file_or_dir is a .crt file path or directory that contains .crt files.
    local _source=${1:?"please specify certificate source file/folder."}
    source /etc/os-release
    local local_trust_store
    case $ID_LIKE in
    debian) local_trust_store=/usr/local/share/ca-certificates/ ;;
    rhel) local_trust_store=/etc/pki/ca-trust/source/anchors/ ;;
    *)
        log error "unknown system ${ID_LIKE}/${ID}."
        return 1
        ;;
    esac

    if [ -d "$_source" ]; then
        local certfile
        for certfile in "$_source"/*.crt; do
            #* cp: cannot stat './*.crt': No such file or directory,
            #* we must check the existence of certificate files.
            [ -e "$certfile" ] || break
            cp -v "$certfile" "$local_trust_store"
        done
    elif [ -e "$_source" ]; then
        cp "$_source" "$local_trust_store"
    else
        log error "no such file or directory ${_source}! skip installation."
        return 1
    fi

    case $ID_LIKE in
    debian) update-ca-certificates ;;
    rhel)
        update-ca-trust force-enable
        update-ca-trust extract
        ;;
    esac
}
