#!/bin/bash -e

function load() {
    # local _SCRIPTDIR_ to avoid pollute caller environment.
    local _SCRIPTDIR_
    _SCRIPTDIR_=$(dirname "${BASH_SOURCE[0]}")
    if ! command -v log >/dev/null; then
        source "${_SCRIPTDIR_}/logging.sh"
    fi
    if ! command -v create_progress_bar >/dev/null; then
        source "${_SCRIPTDIR_}/bash_progress_bar/progress_bar.sh"
    fi
    if ! command -v docker >/dev/null; then
        if command -v podman >/dev/null; then
            log info "use podman to replace docker ($(alias -p docker))."
        else
            log error "cannot find docker CLI, you may not enable docker on the machine".
            exit 1
        fi
    fi
    # _docker_version_info will be brought to the global scope
    # so we can refer the same variable, each time we call this function.
    if [ -z "$_docker_version_info" ]; then
        if _docker_version_info=$(docker version 2>&1); then
            log debug --prefix '> ' 'docker version info' "$_docker_version_info"
        else
            local _result=$?
            log error --prefix '! ' "$_docker_version_info"
            _docker_version_info=''
            return $_result
        fi
    fi
}
load

# function _validate_docker_engine(){}

function get_network_addresses() {
    if [[ $# -eq 0 || "$1" = '-h' || "$1" = '--help' ]]; then
        echo "usage: get_network_addresses --filter driver=bridge --name 'net$'"
        return 1
    fi
    local opt_filter opt_name
    while [ -n "$1" ]; do
        case "$1" in
        --filter) opt_filter=(--filter "$2") ;;
        --name) opt_name=$2 ;;
        esac
        shift 2
    done

    local net
    for net in $(docker network ls "${opt_filter[@]}" --quiet); do
        if [ -n "$opt_name" ]; then
            if ! docker network inspect "$net" --format "{{.Name}}" | grep -E "${opt_name}" >/dev/null; then
                continue
            fi
        fi
        if command -v jq >/dev/null; then
            docker network inspect "$net" | jq '.[] | {Name: .Name, Network: .IPAM.Config}'
        else
            docker network inspect "$net" --format "{{json .IPAM.Config}}"
        fi
    done
}

# Find if an image exists in local storage
#
# Parameters
# - image: image reference as REPOSITORY[:TAG], can containing wildcard.
# - keyword: pattern in image's REPOSITORY, TAG,..., but not the concatenated pattern.
#   by default, using the first parameter's REPOSITORY part as keyword.
function find_image() {
    local image="$1" image_name
    image_name=$(sed -E 's/(.*):.*/\1/' <<<"$image")
    local keyword=${2:-"$image_name"}
    #* docker image ls will always return 0, even if no matched image is found.
    #* so we need to use grep to filter the results.
    # grep fails when nothing matched
    docker image ls "$image" | grep -E "$keyword" >/dev/null
}

# run shell command from docker image to get the system info of the image.
#
# field name to retrieve:
# os: linux os distribution name, e.g.,
#+    ol, rocky, rhel, centos, debian, ubuntu,...;
# os_version: os version, human friendly code name is preferred, e.g.,
#+    jammy, bookworm, 7, 9.3, ...
# os_version_id: numeric version, i.e., VERSION_ID, e.g.,
#+    22.04, 12, 7, 9.3
# os_type: e.g., rhel, debian,...
## os-release version info
#>               VERSION_ID ID       ID_LIKE              VERSION_CODENAME VERSION
#> oraclelinux   "9.4"      "ol"     "rhel fedora"         -                9.4
#> rockylinux    "9.3"      "rocky"  "rhel centos fedora"  -               "9.3 (Blue Onyx)"
#> rhel          "9.3"      "rhel"   "fedora"              -               "9.3 (Plow)"
#> centos-stream "9"        "centos" "rhel fedora"         -               "9"
#> centos7       "7"        "centos" "rhel fedora"         -               "7 (Core)"
#> debian        "12"        debian   -                    bookworm        "12 (bookworm)"
#> ubuntu        "22.04"     ubuntu   debian               jammy           "22.04.4 LTS (Jammy Jellyfish)"
function get_image_info() {
    local property=$1 && shift
    local image=$1 && shift
    declare -a opt_platform
    if [ $# -ge 1 ]; then
        if [ -n "$1" ]; then
            opt_platform=(--platform "$1")
        fi
        shift
    fi
    local output
    # shellcheck disable=SC2016
    case $property in
    os) command='source /etc/os-release && echo ${ID:-${ID_LIKE}}' ;;
    os_version)
        command='source /etc/os-release && echo ${VERSION_CODENAME:-${VERSION_ID}}'
        ;;
    os_version_id)
        command='source /etc/os-release && echo ${VERSION_ID}'
        ;;
    os_type)
        command='source /etc/os-release && sed -E -e "s/.*(fedora|rhel).*/rhel/" <<<"${ID_LIKE:-$ID}"'
        ;;
    user)
        command="cat /etc/passwd | grep -E '/(home|config)' | awk -F: '{print \$1}'"
        ;;
    *)
        log error "property ${property} is not supported!"
        exit 1
        ;;
    esac
    output=$(
        docker run \
            "${opt_platform[@]}" \
            --rm --entrypoint=/bin/bash \
            "${image}" \
            -c \
            "$command"
    )
    if [ -z "${output}" ]; then
        log error "command failed to retrieve ${property} from ${image}."
        exit 1
    else
        echo "$output"
    fi
}

# syntax: docker_compose_with_profiles command [options] [--] service... 
function docker_compose_with_profiles() {
    if [ -e .env ]; then
        # shellcheck disable=SC1091
        # only import if the file exists at working directory.
        . ./.env
    fi
    #* COMPOSE_ENV_FILES should be specified before execute docker compose.
    # We will preload env files to validate key environment variables.
    if [ -z "${COMPOSE_ENV_FILES}" ]; then
        COMPOSE_ENV_FILES=.env
    fi
    if [[ -n "$ENV" && -e ".$ENV.env" ]]; then
        if ! grep ".$ENV.env" <<<"$COMPOSE_ENV_FILES" >/dev/null; then
            COMPOSE_ENV_FILES="$COMPOSE_ENV_FILES,.$ENV.env"
        fi
        # shellcheck disable=SC1090
        # only import if the file exists at working directory.
        . "./.$ENV.env"
    fi
    log info "COMPOSE_ENV_FILES=${COMPOSE_ENV_FILES}"
    export COMPOSE_ENV_FILES

    local compose_files
    #* COMPOSE_FILE is a environment variable recognized by docker-compose, whose value
    #* is a list YAML files separated by ":". We will check the format for COMPOSE_FILE
    #* or if one of the default compose files exist.
    if [ -z "$COMPOSE_FILE" ]; then
        compose_files=()
        local f
        for f in {docker-,}compose.{yaml,yml}; do
            if [ -e "$f" ]; then
                compose_files+=("$f")
            fi
        done
        IFS=':' log info "COMPOSE_FILE is not specified, loading default: ${compose_files[*]}"
        if [[ "${#compose_files[@]}" -gt 1 ]]; then
            log error --prefix ' |' "multiple compose files exist in current folder" \
                "please specify the files you want to use as COMPOSE_FILE in .env file." \
                ">>: COMPOSE_FILE=docker-compose.yml:docker-compose.prod.yml"
            log info --prefix '>>:' "available compose files are:" "${compose_files[@]}"
            return 1
        elif [[ "${#compose_files[@]}" -eq 0 ]]; then
            log error --prefix ' |' \
                "no compose files found and you does not supply customized compose file." \
                "Please specify the files you want to use as COMPOSE_FILE in .env file." \
                ">>: COMPOSE_FILE=docker-compose.yml:docker-compose.prod.yml"
            return 1
        fi
    else
        COMPOSE_FILE=${COMPOSE_FILE:-"$COMPOSE_FILES"}
        log info "COMPOSE_FILE=${COMPOSE_FILE}"
        if grep -E ',|;' <<<"$COMPOSE_FILE" >/dev/null; then
            log error "compose files should be separated by ':', please check the configuration."
            return 1
        fi
        # IFS=':' read -a compose_files -r <<<"$COMPOSE_FILE"
        # for ((i = 0; i < ${#compose_files[@]}; ++i)); do
        #     compose_files[$i]=$(sed -E 's/^\s*|\s*$//g' <<<"${compose_files[$i]}")
        # done
    fi

    if [ -z "$COMPOSE_PROFILES" ]; then
        local use_profiles=false
        local file
        for file in "${compose_files[@]}"; do
            if grep -E '^\s*profiles:' "$file" >/dev/null; then
                use_profiles=true
                break
            fi
        done
        if [ "$use_profiles" = true ]; then
            log error --prefix ' |' '' \
                "profiles are used in compose files, COMPOSE_PROFILES must be explicitly" \
                "specified for running the services." \
                ">>: COMPOSE_PROFILES=frontend,debug"
            return 1
        fi
    else
        log info "COMPOSE_PROFILES=${COMPOSE_PROFILES}"
    fi

    local command=${1:-up}
    if [ -n "$1" ]; then
        shift #! shift will return error if no more arguments.
    fi

    declare -a options
    declare -a extra_options
    case $command in
    up)
        options=('-d' '--remove-orphans')
        while [ $# -gt 0 ]; do
            case $1 in
            --reload | --force-recreate) options+=('--force-recreate') ;;
            --) shift && extra_options+=("${@}") && break ;; # does not processing remain arguments
            *) extra_options+=("$1");; # unknown options passing to 'docker compose up'
            esac
            shift
        done
        set -- "${extra_options[@]}"
        ;;
    esac

    # processing remaining options that will be passed to docker compose.
    extra_options=()
    if [[ $1 =~ ^- ]]; then
        while [ $# -gt 0 ]; do
            case $1 in
                --dry-run ) COMPOSE_TEST=true ;;
                --) shift && break ;;
                *) extra_options+=("$1")
            esac
        done
    fi
    if [[ $COMPOSE_TEST =~ on|true ]]; then
        options+=('--dry-run')
    fi
    # remaining positional arguments will be treated as service names

    if [[ $command =~ up|start|restart|down ]]; then
        declare -a _compose_services _extra_compose_services
        if [ -n "$COMPOSE_SERVICES" ]; then
            IFS=, read -r -a _compose_services <<<"$COMPOSE_SERVICES"
            local cli_args
            # convert extra argument list as a multiline text for ease of search
            #
            # the following code is equivalent to cli_args=$(IFS=$'\n'; echo "${*}")
            # inside $(...), multiple command allowed on each line, use \<newline> to continue line.
            cli_args=$(
                IFS=$'\n'
                echo "${*}"
            )
            local s
            for s in "${_compose_services[@]}"; do
                if ! grep "$s" <<<"$cli_args"; then
                    _extra_compose_services+=("$s")
                fi
            done
            if [ ${#_extra_compose_services[@]} -gt 0 ]; then
                IFS=','
                log info "COMPOSE_SERVICES=${_extra_compose_services[*]} (extra compose services, only effect if its profile is activated.)"
                IFS=
            fi
        fi
    fi

    declare -a compose
    if docker compose version >/dev/null 2>&1; then
        compose=(docker compose)
    elif command -v docker-compose >/dev/null; then
        compose=(docker-compose)
    else
        log error "docker compose is not available, check your docker installation."
        return 1
    fi

    set -x +e
    "${compose[@]}" "${extra_options[@]}" "$command" "${options[@]}" "${_extra_compose_services[@]}"
    local result=$?
    { set +x -e; } 2>/dev/null
    if [ $result -ne 0 ]; then
        log error --prefix ' ! ' \
            "docker compose $command failed!" \
            "$(docker compose ps -a)"
        return $result
    fi
}

function _validate_export_path() {
    local project_path=${1:?'please specify project path (argument 1)!'}
    local type=${2:-'COMPOSE'}
    if [ -e "$project_path/.env" ]; then
        # shellcheck disable=SC1091
        # only import if the file exists at working directory.
        . "$project_path/.env"
    else
        # skip loading environment variables from the "$project_path".
        # env variables must be already loaded by another call to _validate_export_path
        # with different path, or explicitly set in the code.
        log warn ".env file is not found, using existing environment variables."
    fi
    eval "SHUTILS_CONTAINER_EXPORT_PATH=\${EXPORT_${type}_PATH:-\"\${EXPORT_PATH}\"}"
    if [ -z "$SHUTILS_CONTAINER_EXPORT_PATH" ]; then
        log error --prefix ' | ' \
            'SHUTILS_CONTAINER_EXPORT_PATH is not specified!' \
            "Please specify EXPORT_${type}_PATH or EXPORT_PATH in .env file!"
        return 1
    fi
    if [ ! -d "${SHUTILS_CONTAINER_EXPORT_PATH}" ]; then
        log error --prefix '-> ' \
            "output directory <${SHUTILS_CONTAINER_EXPORT_PATH}> does not exist!" \
            "please change the value of EXPORT_${type}_PATH or EXPORT_PATH in .env!"
        return 1
    fi
}

#export registry images (manifest list) with regclient
#
# usage:
#     export_registry_image image_ref
#
# Try to load image-export related environment variables (EXPORT_IMAGE_PATH
# or EXPORT_PATH) from .env file in current working directory.
# Before calling this function, change working directory to one that have
# a .env file, or explicitly set the variables in the code.
function export_registry_image() {
    if [[ -z "$1" || "$1" =~ ^(-h|(--)?help)$ ]]; then
        echo "export registry images (manifest list) with regclient"
        echo "usage:"
        echo ">>:    export_registry_image image_ref"
        echo ""
        echo "set EXPORT_IMAGE_PATH or EXPORT_PATH, or load them from ${PWD}/.env file."
        return 1
    fi

    declare -a parsed_args=()
    local if_skip_exist=${SHUTILS_CONTAINER_EXPORT_IMAGE_SKIP_EXIST:-false}
    while [ $# -gt 0 ]; do
        case $1 in
        --skip-exist) if_skip_exist=true ;;
        *) parsed_args+=("$1") ;;
        esac
        shift
    done
    set -- "${parsed_args[@]}"

    local image_ref=$1
    if ! _validate_export_path . 'IMAGE'; then
        return 1
    fi
    if ! command -v regctl >/dev/null; then
        log error --prefix '! ' \
            "regctl not found, please first install regclient: " \
            ">>: ./bin/shutils/installer.sh regclient"
        return 1
    fi
    image_archive_name="${SHUTILS_CONTAINER_EXPORT_PATH}/$(sed -E 's|[/:]|_|g' <<<"$image_ref").tar"
    if [[ -e "$image_archive_name" && "$if_skip_exist" = true ]]; then
        log info "$image_archive_name exist, skip."
        return 0
    fi
    set -x
    regctl image export "$image_ref" "${image_archive_name}"
    { set +x; } 2>/dev/null
}

# usage: export_compose_images [project_path] [options]
#
# project_path: the path of the compose project, default is current directory.
#
# options:
# --cli CLI    specify use regclient(regctl) or docker to export images.
# --compose-file FILE
#              specify compose file that contains export images, the file path
#              is relative to the project_path.
# --skip-exist skip export if the image has already existed.
function export_compose_images() {
    if [[ -z "$1" || "$1" =~ ^(-h|(--)?help)$ ]]; then
        echo "export images used in docker compose project"
        echo "usage:"
        echo ">>:    export_compose_images [project_path] \\"
        echo ">>:        --cli regctl \\"
        echo ">>:        --compose-file compose.yml \\"
        echo ">>:        --skip-exist"
        echo ""
        echo "set EXPORT_IMAGE_PATH or EXPORT_PATH, or load them from ${PWD}/.env file."
        return 1
    fi
    declare -a parsed_args=()
    local export_cli compose_file
    export SHUTILS_CONTAINER_EXPORT_IMAGE_SKIP_EXIST=false
    while [ $# -gt 0 ]; do
        case $1 in
        --cli) export_cli=$2 && shift ;;
        --compose-file) compose_file=$2 && shift ;;
        --skip-exist) SHUTILS_CONTAINER_EXPORT_IMAGE_SKIP_EXIST=true ;;
        *) parsed_args+=("$1") ;;
        esac
        shift
    done
    set -- "${parsed_args[@]}"

    local project_path=${1:-'.'} && if [ $# -gt 0 ]; then shift; fi
    if ! _validate_export_path "$project_path" 'IMAGE'; then
        return 1
    fi
    compose_file="${project_path}/${compose_file:-compose.yml}"

    if ! command -v regctl >/dev/null; then
        if [[ "$export_cli" =~ regctl|regclient ]]; then
            log error --prefix '! ' \
                "regctl not found, please first install regclient: " \
                ">>: ./bin/shutils/installer.sh regclient"
            exit 1
        elif [ -z "$export_cli" ]; then
            log warning --prefix '* ' \
                "regctl is not found, exporting image of current platform (linux/$(arch)) with 'docker save'."
        fi
    elif [ -z "$export_cli" ]; then
        export_cli=regctl
    fi

    local image_list
    image_list=$(sed -En 's/^\s*image:\s+([^\s].*)(\s*#.*)?$/\1/p' "${compose_file}" | sort | uniq)
    readarray -t images <<<"${image_list}"
    local image_canonical_name image_archive_name output i _registry
    for i in "${images[@]}"; do
        #* interpolate env vars inside the image name, like:
        #+   ${CONTAINER_REGISTRY:-docker.io/library}/coder-offline:${CODER_VERSION:-latest}
        #> environment variables are loaded via '_validate_export_path "$project_path"'
        image_canonical_name=$(eval echo "$i")

        # convert hierarchy image name into flat file name, e.g.,
        #>   registry.coderdev.com/coder-offline:v2.12.3 -> registry.coderdev.com_coder-offline_v2.12.3
        image_archive_name=$(sed -E 's|[/:]|_|g' <<<"$image_canonical_name")
        output="${SHUTILS_CONTAINER_EXPORT_PATH}/$image_archive_name.tar"
        if [ -e "$output" ]; then
            log info "the image has already existed at ${output}."
            continue
        fi

        if [[ "$export_cli" =~ regctl|regclient ]]; then
            # trying export from specified container registry.
            _registry=$(sed -En 's|(.*?\..*?)/.*|\1|p' <<<"$image_canonical_name")
            log info "exporting ${image_canonical_name} from container registry [${_registry:-docker.io}]..."
            if ! export_registry_image "$image_canonical_name"; then
                return 1
            fi
        else
            log info "exporting ${image_canonical_name} from local image store ..."
            if [[ "$SHUTILS_CONTAINER_EXPORT_IMAGE_SKIP_EXIST" = true && -e "$output" ]]; then
                log info "the image has already existed at ${output}."
                continue
            fi
            # if the image is not in local store, docker will try to pull it from remote registry.
            set -x
            docker save --output "${output}" "${image_canonical_name}"
            { set +x; } 2>/dev/null
        fi
    done
}

function usage_export_compose_project() {
    echo "usage:"
    echo "    export_compose_project project_path --exclude PATTERN ..."
    echo "there are build in exclude list for all projects."
    echo ""
    echo "Note on filtering files:"
    echo "Tar only support excluding files, use exclude patterns carefully."
}

function export_compose_project() {
    if [[ -z "$1" || "$1" = '-h' || "$1" = '--help' ]]; then
        usage_export_compose_project 2>&1 && return 1
    fi
    local project_folder project_path project_name DATE file_info
    project_folder=${1} && shift
    project_path=$(realpath "$project_folder")
    project_name=$(basename "$project_path")

    _validate_export_path "$project_path" 'COMPOSE'
    DATE=$(date +%Y%m%d)

    declare -a extra_exclude_list
    while [ -n "$1" ]; do
        case $1 in
        --exclude) extra_exclude_list+=("$1" "${2:?missing value for option ${1}.}") && shift 2 ;;
        esac
    done
    if [ -e "$project_path/.exclude" ]; then
        extra_exclude_list+=(--exclude-from "$project_path/.exclude")
    fi

    cd "$project_path/.." || return 1
    ln -sf --no-dereference "$project_name" "$project_name-$DATE"
    local target="${SHUTILS_CONTAINER_EXPORT_PATH}/${project_name}-$DATE.tar.gz"
    log "exporting the compose project <${project_name}> to ${target}."
    set -x
    tar -czf "${target}" \
        --dereference \
        --exclude ".crossnote" \
        --exclude ".gitmodules" \
        --exclude ".vscode" \
        --exclude "__pycache__" \
        --exclude "*.bak" \
        --exclude "*.log" \
        "${extra_exclude_list[@]}" \
        "${project_name}-$DATE"
    { set +x; } 2>/dev/null
    rm -f "${project_name}-$DATE"
    cd "$OLDPWD" || return 1 #* $PWD and $OLDPWD are builtin environment variables
    file_info=$(stat "$target")
    log info --prefix '>>: ' "exported archive file information:" "${file_info}"
}

# TODO image store should be set by user and checked in the exporting/importing functions
image_store="${HOME}/backup/container/images"

function backup_image_store() {
    # backup images in local store
    local image_list all_images image_file
    image_list=$(
        docker images --format "{{.Repository}}:{{.Tag}}" |
            grep -v '<none>' | sort | uniq
    )
    readarray -t all_images <<<"$image_list"
    # used images only
    # local used_image_list used_images
    # used_image_list=$(
    #     docker ps --all --format "{{.Image}}" |
    #         sort | uniq |
    #         xargs -I {} docker image inspect --format='{{.RepoTags}}' {} |
    #         sed -En 's/\[(.+)\]/\1/p'
    # )
    # readarray -t used_images <<<"$used_image_list"

    image_store=${IMAGE_BACKUP_STORE:-$image_store}
    #* only create the last level of folder, in case you specify wrong folder path!
    if [ ! -e "$image_store" ]; then
        if ! mkdir "$image_store"; then
            log warning --prefix ' | ' \
                "please ensure the folder $image_store is your backup folder. " \
                "create the parent folder by yourself, e.g., " \
                "> mkdir -p $(dirname "$image_store")" \
                "and then run" \
                "> export IMAGE_BACKUP_STORE=$image_store" \
                "> backup_image_store" \
                "or put this variable to \`~/.bashrc\`."
            return 1
        fi
    fi
    create_progress_bar --eta --trap -N "${#all_images[@]}" Exporting
    declare -i i=1
    local image
    for image in "${all_images[@]}"; do
        image_file=$(sed -E 's![:/]!_!g' <<<"$image")
        log info "exporting $image as $image_store/${image_file}.tar ......"
        draw_progress_bar $((i++)) "$image"
        if [ -e "$image_store/${image_file}.tar" ]; then
            log warning "${image_file}.tar already exists."
            # TODO compare image create time and the exported archive create time
            # if the former is greater that that of the exported archive, then update the archive.
            # get the image name and the creation time:
            #> docker image ls --format "{{.Repository}}:{{.Tag}}={{.CreatedAt}}" | sed -E 's/\s*[A-Z]{3}$//'
            # get the timestamp of the creation time:
            #> date -d 'date_string' +%s
            # get the archive file's modification time:
            #> stat --format '%Y %y' whoami.tar  # (timestamp or date string)
        else
            set -x
            docker save --output "$image_store/${image_file}.tar" "$image" 1>&2
            { set +x; } 2>/dev/null
        fi
        echo "$image"
    done >"${image_store}/image.list"
    destroy_scroll_area
}

function recover_image_store() {
    # recovery images to local store from backup
    local image_file image_name n_images

    image_store=${IMAGE_BACKUP_STORE:-$image_store}
    if [ ! -e "$image_store" ]; then
        log error "image store <${image_store}> does not exist!"
        exit 1
    fi
    #* wc -l count number of newlines, so the image.list should ends with newline
    #* for accurately count lines of text.
    n_images=$(wc -l "${image_store}/image.list" | awk '{print $1}')

    create_progress_bar --eta --trap -N "$n_images" Importing
    declare -i i=1
    while read -r image; do
        image_file=$(sed -E 's![:/]!_!g' <<<"$image")
        image_name=$(sed -E 's/(.*):.*/\1/' <<<"$image")
        log info "import $image from $image_store/${image_file}.tar ......"
        draw_progress_bar $((i++)) "$image"
        if find_image "$image" "$image_name"; then
            #> as we only have the creation time (not the import time) of image
            #> we cannot compare it with the modification time of the archive.
            #> so we can only skip the existing images. (The creation time of
            #> the image will always less than the modification time of the
            #> archive).
            log warning "$image exists in local image store, skip!"
        else
            if [ -e "$image_store/${image_file}.tar" ]; then
                set -x
                docker load --input "$image_store/${image_file}.tar"
                { set +x; } 2>/dev/null
            else
                log warning "${image_file}.tar does not exist, skip!"
            fi
        fi
    done <"${image_store}/image.list"
}

function sync_image() {
    local image_filter
    while [ $# -gt 0 ]; do
        case $1 in
        --filter) image_filter=$2 && shift ;;
        *) break ;;
        esac
        shift
    done
    local image_list_file=${1:?"please specify a image list file."}
    if [ -e .env ]; then
        # shellcheck disable=SC1091
        . .env
    fi
    local image_registry=${2:-${CONTAINER_REGISTRY:?""}}
    if [ -z "$image_registry" ]; then
        log error "default image registry is not specified as \$2 or CONTAINER_REGISTRY (in .env)!"
        exit 1
    fi

    declare -a image_sync_specs
    readarray -t image_sync_specs <"$image_list_file"

    if ! command -v regctl >/dev/null; then
        log error "regctl is not installed, please run \`installer.sh regclient\`!"
        return 1
    fi

    local source_image target_image operation _registry _image auth_registry_list
    for spec in "${image_sync_specs[@]}"; do
        if grep -E '\s*#' <<<"$spec" >/dev/null; then
            continue
        fi
        if [ -n "$image_filter" ] && ! grep "$image_filter" <<<"$spec" >/dev/null; then
            log debug "$spec does not match $image_filter, skip!"
            continue
        fi

        # initialize variables in case that read does reset theme if input is missing.
        source_image=''
        target_image=''
        operation=''
        read -r source_image target_image operation <<<"$spec"
        if [ -z "$source_image" ]; then
            continue
        fi
        IFS=/ read -r _registry _image <<<"$target_image"
        if ! grep -E '\.' <<<"$_registry" >/dev/null; then
            # target image is not prefixed with container image registry's domain name (or IP address)
            # add the default container image registry.
            _registry=$image_registry
            target_image="$image_registry/$target_image"
        fi

        #* perform login: password is required to be input by the user.
        #>   regctl registry config # show logged in registry config
        if ! grep "$_registry" <<<"$auth_registry_list" >/dev/null; then
            local regctl_config_file=~/.regctl/config.json
            if [ ! -e "$regctl_config_file" ] && [ -e './config/regctl.json' ]; then
                log info "use template configuration for regclient."
                mkdir -p "$(dirname "$regctl_config_file")"
                cp './config/regctl.json' "$regctl_config_file"
            fi
            if [ -e "$regctl_config_file" ] && [ "$(jq ".hosts[\"$_registry\"].pass" "$regctl_config_file")" != null ]; then
                log debug "$_registry has been logged in with user $(jq ".hosts[\"$_registry\"].user" "$regctl_config_file")."
            elif [ "$_registry" = "$image_registry" ]; then
                if [ -n "$CONTAINER_REGISTRY_USER" ]; then
                    log info "login into $image_registry with user $CONTAINER_REGISTRY_USER ..."
                    regctl registry login "$image_registry" --user "$CONTAINER_REGISTRY_USER"
                else
                    log info "Access $image_registry without login."
                fi
            else
                # all other registries need login to upload images
                log info "login into $_registry ......"
                regctl registry login "$_registry"
            fi
            auth_registry_list="$auth_registry_list:$_registry"
        fi

        if regctl manifest get "$target_image" >/dev/null 2>&1; then
            if [ "$operation" = 'skip_on_exist' ]; then
                log info "$target_image already exists, skip."
                continue
            fi
        fi
        log info "copy image from $source_image to $target_image ......"
        regctl image copy "$source_image" "$target_image"
    done
}

[[ "$0" =~ ^- ]] && PROGNAME=bash || PROGNAME=$(basename "$(realpath "$0")")
if [[ "$PROGNAME" = 'container.sh' ]]; then
    case $1 in
    imagestore)
        case $2 in
        backup) backup_image_store ;;
        recover) recover_image_store ;;
        *) log error "not supported command <$1 $2>!" && exit 1 ;;
        esac
        ;;
    image)
        case $2 in
        export) export_registry_image "${@:3}" ;;
        sync) sync_image "${@:3}" ;;
        *) log error "not supported command <$1 $2>!" && exit 1 ;;
        esac
        ;;
    compose)
        case $2 in
        export) export_compose_project "${@:3}" ;;
        export-image) export_compose_images "${@:3}" ;;
        *) docker_compose_with_profiles "${@:2}" ;;
        esac
        ;;
    fix)
        # fix docker desktop emulation with QEMU that leading to
        # 'qemu: uncaught target signal 11 (Segmentation fault) - core dumped' or
        # 'exec /bin/sh: exec format error'.
        #
        #* the command needs to be run every time docker engine is restarted.
        docker run --rm --privileged --pull always \
            multiarch/qemu-user-static --reset -p yes -c yes
        ;;
    *) log error "not supported command <$1>!" && exit 1 ;;
    esac
fi
