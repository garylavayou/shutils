# Installation Techniques

- Binary release: download from official release page, and extract or copy files to install
  location with proper execution permission set.

- Official install script: download the official install script and perform installation online.
- Package manager: using `apt` / `yum` (`dnf`) / `brew` / `npm`, etc. to install package from
  package repositories.
  - Third-party repositories should be added in advance.
  - Non-system package manager, such as `brew` and `npm`, need to installed at first.
