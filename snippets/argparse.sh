#!/bin/bash

function simple_parse(){
    local default_version version_hint

    while [ -n "$1" ]; do
        case "$1" in
        --default) default_version=$2 && shift ;;
        --hint) version_hint=$2 && shift ;;
        -*) log error "unknown option $1, exit!" && return 1 ;;
        *) break ;;
        esac
        shift 1
    done

    echo "optional arguments: --default=$default_version --hint=$version_hint"
}

function getopt_parse(){
    local SHORT='Dhrp:p:R:'
    local LONGS="debug,help,recursive,platform:,os:,repository:,registry:"
    local PARSED
    # use "$FUNCNAME[0]" or "$0" as pasrser name
    PARSED=$(getopt --options=$SHORT --longoptions=$LONGS --name "${FUNCNAME[0]}" -- "$@")
    eval set -- "$PARSED"

    while true; do
        if [[ "$1" =~ ^--?[^-]+ ]]; then
            if [[ "$2" =~ ^--? ]]; then
                log debug "input option < $1 >."
            else
                log debug "input option < $1=$2 >."
            fi
        fi
        case "$1" in
        -D | --debug) export LOG_LEVEL=DEBUG ;;
        -h | --help) usage && return 1 ;;  # or exit 1
        -R | --repository) export CONTAINER_REGISTRY=$2 && shift ;;
        -r | --recursive) export BUILD_OPT_RECURSIVE=true ;;
        -p | --platform) export ARCH_PLATFORMS=$2 && shift ;;
        --os) os_name="$2" && shift ;;
        --) shift && break ;; # break the while loop
        esac
        shift
    done

    echo "debug: os name: ${os_name}."
}

function test_debug_opts(){
    test "$(grep -E '^--?[^-]+' <<<'--help')" = '--help'
    test "$(grep -E '^--?[^-]+' <<<'-h')" = '-h'
    test "$(grep -E '^--?[^-]+' <<<'--')" = ''
    test "$(grep -E '^--?[^-]+' <<<'help')" = ''
}