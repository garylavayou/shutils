#!/bin/bash -e

function load() {
    # local _SCRIPTDIR_ to avoid pollute caller environment.
    local _SCRIPTDIR_
    _SCRIPTDIR_=$(dirname "$(realpath "${BASH_SOURCE[0]}")")
    if ! command -v log >/dev/null; then
        source "${_SCRIPTDIR_}/logging.sh"
    fi
}
load

# checking the access time update option of the filesystem of the cache folder.
# if the filesystem does not support access time update, return `false`.
function check_filesystem_atime_option() {
    local file_dir=${1:?}
    local filesystem
    filesystem=$(df -hT "$file_dir" | grep '/' | awk '{print $1}')
    if mount -l | grep -E "${filesystem}.*noatime" >/dev/null; then
        log error "$file_dir (on $filesystem) does not enable access time update, cannot determine outdated files!"
        return 1
    elif ! mount -l | grep -E "${filesystem}.*relatime" >/dev/null; then
        log warning "$file_dir (on $filesystem) does not enable relatime option, the access time update may introduce high overhead!"
    fi
}

# clean outdated files according to its last access time
#
# About the file time properties:
# mtime (%Y) may be altered by download program.
# ctime (%Z) is filesystem native time about property/content modification.
# atime (%X) is the real access time or inherit from mtime/ctime (with filesystem
# relatime option enabled).
function clean_outdated_files() {
    if [[ "$1" = '-h' || "$1" = '--help' || "$1" = 'help' ]]; then
        echo "usage: clean_outdated_files --lifetime DAYS --filetype TYPES,... DIRECTORY'" 1>&2
        return 1
    fi

    local cache_lifetime=365 # days
    local filetypes opt_dry_run
    declare -a parsed_args
    while [ -n "$1" ]; do
        case "$1" in
        --lifetime) cache_lifetime="$2" && parsed_args+=("$1" "$2") && shift ;;
        --filetype) filetypes="$2" && parsed_args+=("$1" "$2") && shift ;;
        --dry-run) opt_dry_run=true && parsed_args+=("$1") ;;
        -*) log error "unknown option $1, exit!" && return 1 ;;
        *) break ;;
        esac
        shift 1
    done
    local file_dir=${1:?"File directory needs to be specified, see 'clean_outdated_files help'"}
    local file_ext_pattern
    if [ -n "$filetypes" ]; then
        local file_exts
        IFS=, read -r -a file_exts <<<"$filetypes"
        file_ext_pattern="\\.($(
            IFS='|'
            echo "${file_exts[*]}"
        ))\$"
    else
        file_ext_pattern='.*'
    fi

    check_filesystem_atime_option "$file_dir"

    local file file_time file_duration
    for file in "$file_dir"/*; do
        file_time=$(stat --format '%x' "$file")
        file_duration=$((($(date +%s) - $(stat --format '%X' "$file")) / 86400))
        if [ -f "$file" ]; then # both real file and symbolic links are supported
            if grep -E "$file_ext_pattern" <<<"$file" >/dev/null; then
                if [ $file_duration -gt "$cache_lifetime" ]; then
                    log info "file $file access time (${file_time:0:19}) exceed valid cache period ($file_duration >= ${cache_lifetime}d), remove."
                    if [ "$opt_dry_run" = true ]; then
                        echo "+ rm -vf ${file} (${file_time:0:19})" 1>&2
                    else
                        rm -vf "$file"
                    fi
                else
                    log debug "file $file access time (${file_time:0:19}) is within valid cache period ($file_duration < ${cache_lifetime}d), skip."
                fi
            else
                log debug "file $file does not match extension '$file_ext_pattern', skip."
            fi
        elif [ -d "$file" ]; then
            log info "clean subdirectories of ${file} ......"
            clean_outdated_files "${parsed_args[@]}" "$file"
            if [ $file_duration -gt "$cache_lifetime" ]; then
                log info "directory $file access time (${file_time:0:19}) exceed valid cache period ($file_duration >= ${cache_lifetime}d), clean contents."
                if [ "$opt_dry_run" = true ]; then
                    echo "+ rmdir -v $file (${file_time:0:19})" 1>&2
                else
                    if ! rmdir -v "$file" 2>/dev/null; then
                        log info "directory is not empty, cannot be cleaned." 1>&2
                    fi
                fi
            else
                log debug "directory $file access time (${file_time:0:19}) is within valid cache period ($file_duration < ${cache_lifetime}d), skip."
            fi
        else
            log warning "file $file is a special file, cannot be deleted!"
        fi
    done
}
