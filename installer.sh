#!/bin/bash -e

# shellcheck disable=SC2155
# Declare and assign separately to avoid masking return values.

# shell script functions to install necessary tools for development
# the functions may containing manual install method or method using package
# managers.

# loading scripts installed on PATH
#
#> the load functions does not depend on the execution context, so that it can
#> be installed as a basic shell function in the system. For example, add it to
#> ~/.bash_profile or /etc/profile.d/shell.sh (as executable).
#
# If need not to avoid repeated importing, just calling `source` instead of load
# is adequate. 
function load() {
    if [ -z "$2" ] || ! command -v "$2"; then
        # shellcheck disable=SC1090
        # ShellCheck can't follow non-constant source.
        . "${1:?}" && command -v "${2:-command}"
    fi >/dev/null
}

load shell.sh vcmp
load logging.sh log

# Get user friendly name and type of the os distribution
#* _os_name and _os can be referred after calling this function
#* this function only for internal use.
function _get_os_name_version() {
    _os_name=$(get_os_name)
    _os=$(get_os_type)
}

# Get OS version number from VERSION_ID, which is numeric notated for all major
# distributions (see examples in <container.sh>)
function get_os_version_number() {
    local _os_version=$(
        . /etc/os-release
        echo "$VERSION_ID"
    )
    echo "$_os_version"
}

# Get OS pretty name, e.g., "Ubuntu 22.04.4 LTS", "Rocky Linux 9.3 (Blue Onyx)"
function get_os_name() {
    local _os_name=$(
        . /etc/os-release
        echo "$PRETTY_NAME"
    )
    echo "$_os_name"
}

function get_os_type() {
    # OS code name, currently only recognize rhel, fedora, and debian.
    # see also `container.sh#get_image_info()`.
    #> "grep -o" will output multiple lines if multi-matches are found on the
    #> same input line, we only reserve the first match.
    local _os_type
    if _os_type=$(
        . /etc/os-release
        grep -Eo 'rhel|fedora|debian' <<<"${ID_LIKE:-$ID}"
    ); then
        declare -a _os_aliases
        readarray -t _os_aliases <<<"$_os_type"
        echo "${_os_aliases[0]}"
    else
        log error "cannot find os code name from '$(
            . /etc/os-release
            echo "$ID_LIKE"
        )'!"
        return 1
    fi
}

# mapping os arch to container used arch notation.
# os arch series: x86_64/aarch64/...
# container arch: amd64/arm64/...
function _get_mapped_container_arch() {
    local _arch
    _arch=$(arch)
    case $_arch in
    x86_64) _arch=amd64 ;;
    aarch64) _arch=arm64 ;;
    esac
    echo "$_arch"
}

# mapping os arch to app used arch notation that does not fully conform to
# container arch notations.
# os arch series: x86_64/aarch64/...
# container arch: amd64/arm64/...
function _get_mapped_app_arch() {
    #! dicts cannot be access outside of the loading functions in other scripts
    #! for bash version < 4.4, declare -Ag is not supported.
    #! so define it inside this function, which is the only place that refers
    #! these data.

    # shellcheck disable=SC2034
    declare -A custom_arch_alias_x86_64=(
        ['x86_64']=''
        ['x64']='nodejs'
        ['amd64']=''
    )
    # shellcheck disable=SC2034
    declare -A custom_arch_alias_aarch64=(
        ['arm64']='lazydocker,nodejs'
        ['amd64']=''
    )

    local _app=${1:?}
    local _arch=$(arch)
    case $_arch in
    x86_64 | aarch64)
        if vcmp "$(bash_version)" -ge '4.3'; then
            declare -n _aliases="custom_arch_alias_${_arch}"
        else
            declare -A _aliases
            dict_copy "custom_arch_alias_${_arch}" _aliases
        fi
        local a app_list
        for a in "${!_aliases[@]}"; do
            app_list="${_aliases[$a]}"
            if grep "${_app}" <<<"$app_list" >/dev/null; then
                _arch=$a
                break
            fi
        done
        ;;
    esac
    echo "$_arch"
}

function on_interrupt_download() {
    # most often used, use it to perform clean on certain operation.
    echo "" 1>&2
    echo "Interrupt from keyboard (Ctrl+C), receive SIGINT(2)" 1>&2
    if [ -e "$_installer_wget_document" ]; then
        echo "Clean download file $_installer_wget_document ......" 1>&2
        rm -vf "$_installer_wget_document"
    fi
    exit 2
}

# usage: _download_installer URL [FILENAME] [options]
#
# Input:
#   FILENAME (optional):
#     If the download_url does not ends with an expected filename, then
#     explicitly set the filename to save the download content.
#
# Options:
#   How to avoid repeatedly downloading the same file that exists in local
#   cache.
#   --newer     only download the file if the remote resource is newer than the
#               local file. This is useful for remote resource that might be
#               updated.
#   --continue  then only download the file if the remote resource's size is
#               greater than the local file. This is useful if the remote
#               resource will not change, and we whish to continue a previous
#               interrupted download.
#   The above options are only valid if FILENAME is not given, only can only be
#   specified once (the former option will be overridden by the later options).
#   If none of the options are given, then downloading will be skipped if the
#   resource exist in local.
#
# Output:
#   the full path of the downloaded file.
function _download_installer() {
    local _wget_download_mode
    declare -a parsed_args=()
    while [ $# -gt 0 ]; do
        case $1 in
        --continue) _wget_download_mode='continue' ;;
        --newer) _wget_download_mode='timestamping' ;;
        *) parsed_args+=("$1") ;;
        esac
        shift
    done
    set -- "${parsed_args[@]}"

    local _download_cache=${INSTALLER_DOWNLOAD_CACHE:-"${HOME}/.cache"}

    local _release_url=${1:?"error: not specify a download URL."}
    # If the url is not ends with a normal file name (dynamic resource identifier)
    # we must specify a name for it. Otherwise, the resulting filename will be
    # hard to recognize.
    local _release_file=${2:-$(basename "$_release_url")}
    _installer_wget_document="$_download_cache/$_release_file"

    if [[ "$_release_url" =~ ^https://github.com/ ]]; then
        if [ -n "$GITHUB_PROXY" ]; then
            if [[ "$GITHUB_PROXY" =~ /$ ]]; then
                _release_url="$GITHUB_PROXY$_release_url"
            else
                _release_url="$GITHUB_PROXY/$_release_url"
            fi
        fi
    fi
    log debug "download URL: ${_release_url} ..."

    trap on_interrupt_download SIGINT
    declare -a wget_download_options=(--no-verbose)
    if [ -z "$2" ]; then
        wget_download_options+=(--directory-prefix "$_download_cache")
        if [ -z "$_wget_download_mode" ]; then
            # default download mode for github release resources
            if [[ "$_release_url" =~ github.com/.*/releases/latest/download ]]; then
                _wget_download_mode='timestamping'
            elif [[ "$_release_url" =~ github.com/.*/releases/download ]]; then
                _wget_download_mode='continue'
            fi
        fi
        if [ -n "$_wget_download_mode" ]; then
            log debug "wget download mode set to '${_wget_download_mode}'."
            wget_download_options+=("--${_wget_download_mode}")
        else
            log warning "wget download mode is not set."
            wget_download_options+=("--no-clobber")
        fi
        # If downloading failed due to various reason, the partial downloaded
        # file needs to be deleted. Otherwise, when we retry download, the
        # partial downloaded file will be treated as finished, which makes
        # future use of this file doom to fail.
        if explain wget "$_release_url" "${wget_download_options[@]}"; then
            true
        else
            ERROR_CODE=$?
            log error "wget error code: $ERROR_CODE"
            rm -vf "$_installer_wget_document"
            return $ERROR_CODE
        fi
    else
        # --output-document is equivalent to shell redirect operator. Thus,
        # the output "$_installer_wget_document" will be truncated immediately,
        # once wget is started. Hence, using --timestamping/--continue with
        # this option will be meaningless.
        # Also, --output-document cannot be used with --directory-prefix, so we
        # must specify a full path including the directory to save the download
        # file.
        # it will immediately truncate the , so no need to perform clean to
        # avoid corrupted files.
        wget_download_options+=(--output-document "$_installer_wget_document")
        wget "$_release_url" "${wget_download_options[@]}"
    fi 1>&2
    trap - SIGINT

    if [ ! -e "$_installer_wget_document" ]; then
        log error "fail to get resource $_release_file from $_release_url !"
        return 1
    else
        echo "$_installer_wget_document"
    fi
}

# _install_binary_as FILE --prefix PREFIX [PROGNAME]
#
# Output:
#   the installed binary path.
function _install_binary_as() {
    local install_prefix
    while [ -n "$1" ]; do
        case "$1" in
        --prefix) install_prefix=$2 && shift ;;
        -*) log error "unknown option $1, exit!" && return 1 ;;
        *) break ;;
        esac
        shift 1
    done
    local download_file=${1:?"download file path should be specified as \$1!"}
    local progname=${2:-$(basename "$download_file")}

    if [ "$(whoami)" = 'root' ]; then
        install_prefix=${install_prefix:-'/usr/local/bin'}
        local bin_search_path="/usr/local/bin/$progname"
    else
        install_prefix=${install_prefix:-"${HOME}/.local/bin"}
        local bin_search_path="${HOME}/.local/bin/$progname"
    fi

    install --directory "$install_prefix"
    # "install --directory DIRECTORY"... -> "mkdir -p DIRECTORY..."
    install --mode=+x --verbose "--target-directory=$install_prefix" "$download_file" 1>&2
    if [ -n "$progname" ]; then
        ln -svf "$install_prefix/$(basename "$download_file")" "$bin_search_path" 1>&2
    fi
    if [ -e "$bin_search_path" ]; then
        echo "$bin_search_path"
    else
        log error "installation process error: cannot find [$bin_search_path]."
        return 1
    fi
}

# _get_archive_root ARCHIVE
function _get_archive_root() {
    local archive=${1:?"archive file path should be specified as \$1!"}

    local archive_list head_line tail_line
    archive_list=$(tar -tf "$archive")
    head_line=$(head -n1 <<<"$archive_list")
    tail_line=$(tail -n1 <<<"$archive_list")
    if [ "$head_line" = "$tail_line" ]; then
        log warning "no root folder in the archive, only containing single file ${head_line}."
        return 0
    fi

    # retrieve the root folder name by stripping sub-paths
    # some archives (e.g., node) that has sub-folders list the root folder alone
    # as the first item in the listing, while others (e.g., shellcheck) that
    # have only files in root folder, will not list a standalone root folder
    # entry. Nevertheless, the root folder name is the same in both cases.
    # Hence, we can retrieve the root folder components from the listing.
    local head_root tail_root
    head_root=$(sed -E 's!/.*!!' <<<"${head_line}")
    tail_root=$(sed -E 's!/.*!!' <<<"${tail_line}")
    if [ "$head_root" = "$tail_root" ]; then
        echo "$head_root"
    else
        log warn --prefix "* " \
            "no root directory is found in ${archive}." \
            "first file is ${head_line}, last file is ${tail_line}."
    fi
}

# _install_archive_as --prefix PREFIX FILE [PROGNAME]
#   If the archive does not contain a root directory, the install directory
#   will be named as PROGNAME. Otherwise, use the root directory.
#   The main binary file as PROGNAME will be linked under one of the directory
#   on search path. If PROGNAME is missing, using FILE's basename instead as
#   default.
#
# Output:
#   the installed binary path to PROGNAME. If the binary file in the archive
#   cannot be determined by PROGNAME, then the output is empty.
#
# TODO support install multiple version of the same program
#   If version information is embedded as the root folder in the archive, then
#   current implementation will install each version in separate directory.
#*  Otherwise, you need to provide the information separately as an argument
#*  and construct the root folder.
function _install_archive_as() {
    local install_prefix
    while [ -n "$1" ]; do
        case "$1" in
        --prefix) install_prefix=$2 && shift ;;
        -*) log error "unknown option $1, exit!" && return 1 ;;
        *) break ;;
        esac
        shift 1
    done
    local download_file=${1:?"download file path should be specified as \$1!"}
    local progname=${2:-$(basename "$download_file")}

    if [ "$(whoami)" = 'root' ]; then
        install_prefix=${install_prefix:-"/usr/local"}
        local bin_search_path="/usr/local/bin/$progname"
    else
        # normal user must specify directory with write permission
        install_prefix=${install_prefix:-"${HOME}/.local"}
        local bin_search_path="${HOME}/.local/bin/$progname"
    fi

    local root_component=$(_get_archive_root "$download_file")
    if [ -z "$root_component" ]; then
        strip_components=0
        install_prefix="$install_prefix/$progname"
    else
        strip_components=1
        install_prefix="$install_prefix/$root_component"
    fi

    if [ -e "$install_prefix" ]; then
        rm -rf "${install_prefix:?}"/* #* ":?" prevent delete "/*"
    else
        mkdir -p "$install_prefix"
    fi
    set -x
    tar -xf "${download_file}" -C "$install_prefix" --strip-components=$strip_components # -v
    { set +x; } 2>/dev/null

    local bin_path
    if [ -e "$install_prefix/$progname" ]; then
        bin_path="$install_prefix/$progname"
    elif [ -e "$install_prefix/bin/$progname" ]; then
        bin_path="$install_prefix/bin/$progname"
    fi
    if [ -n "$bin_path" ]; then
        # link binary file of the installation into directory in search path
        # and return the binary file's linked path.
        ln -svf "$bin_path" "$bin_search_path" 1>&2
        echo "$bin_search_path"
    else
        log warn "cannot determine package's binary file, you need to add it to PATH."
    fi
}

if [[ $- == *i* ]]; then
    log info "$(basename "${0/EOF/shell}") is in interactive mode."
    INSTALL_ENABLE_DEFAULT_VERSION=${INSTALL_ENABLE_DEFAULT_VERSION:-'true'}
else
    log info "$(basename "${0/EOF/shell}") is not in interactive mode."
    INSTALL_ENABLE_DEFAULT_VERSION=${INSTALL_ENABLE_DEFAULT_VERSION:-'false'}
fi

# syntax: _check_default_version --default latest --hint HINT app VERSION
function _check_default_version() {
    local default_version version_hint
    while [ -n "$1" ]; do
        case "$1" in
        --default) default_version=$2 && shift ;;
        --hint) version_hint=$2 && shift ;;
        -*) log error "unknown option $1, exit!" && return 1 ;;
        *) break ;;
        esac
        shift 1
    done
    local _app_name=${1:?}
    local _version=${2:-${INSTALL_VERSION}}

    if [ -z "$_version" ]; then
        if [ "$INSTALL_ENABLE_DEFAULT_VERSION" = false ]; then
            if [ -n "$version_hint" ]; then
                log error "Specify $_app_name version as \$1 or export INSTALL_VERSION, e.g., $version_hint!"
            else
                log error "Specify $_app_name version as \$1 or export INSTALL_VERSION!"
            fi
            return 1
        else
            _version=${default_version:-'latest'}
            log warning "set $_app_name version to default ($_version)!"
        fi
    fi
    echo "$_version"
}

function install_brew() {
    if command -v brew >/dev/null; then
        log info "$(brew --version) has been installed at $(command -v brew)." \
            "you may try \`brew update && brew upgrade\` to perform upgrade."
        return 0
    fi
    # HOMEBREW_MIRROR="https://mirrors.tuna.tsinghua.edu.cn"
    if [ -z "$HOMEBREW_MIRROR" ]; then
        # install from official site.
        SRC_URL='https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh'
        curl -fsSL "$SRC_URL" | /bin/bash
    else
        export HOMEBREW_BREW_GIT_REMOTE="$HOMEBREW_MIRROR/git/homebrew/brew.git"
        export HOMEBREW_CORE_GIT_REMOTE="$HOMEBREW_MIRROR/git/homebrew/homebrew-core.git"
        export HOMEBREW_BOTTLE_DOMAIN="$HOMEBREW_MIRROR/homebrew-bottles"
        git clone --depth=1 "$HOMEBREW_MIRROR/git/homebrew/install.git" /tmp/brew-install
        bash /tmp/brew-install/install.sh
        rm -rf /tmp/brew-install
        # shellcheck disable=SC2016
        (
            echo
            echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"'
        ) >>"$HOME/.profile"
        if ! grep 'HOMEBREW_BREW_GIT_REMOTE=' "$HOME/.bashrc" >/dev/null; then
            echo "export HOMEBREW_BREW_GIT_REMOTE=$HOMEBREW_BREW_GIT_REMOTE" >>"$HOME/.bashrc"
        fi
        if ! grep 'HOMEBREW_CORE_GIT_REMOTE=' "$HOME/.bashrc" >/dev/null; then
            echo "export HOMEBREW_CORE_GIT_REMOTE=$HOMEBREW_CORE_GIT_REMOTE" >>"$HOME/.bashrc"
        fi
        if ! grep 'HOMEBREW_BOTTLE_DOMAIN=' "$HOME/.bashrc" >/dev/null; then
            echo "export HOMEBREW_BOTTLE_DOMAIN=$HOMEBREW_BOTTLE_DOMAIN" >>"$HOME/.bashrc"
        fi
    fi
}

function install_coder() {
    log warning "install_coder is deprecated! use install_coder_app instead."
    local coder_version
    coder_version=$(_check_default_version --default '2.16.1' coder "${1:-${INSTALL_VERSION}}")
    if curl -fsSL https://coder.com/install.sh |
        sed -E "s|(https://github.com/coder/coder/releases/download)|${GITHUB_PROXY}\1|g" |
        sh -s -- --version "${coder_version:?'failed to resolve coder version to be installed!'}"; then
        true
    else
        ERROR_CODE=$?
        if [ "$ERROR_CODE" -eq 6 ]; then
            log error "network error: please update GITHUB_PROXY ($GITHUB_PROXY)."
        fi
        return $ERROR_CODE
    fi
}

# syntax: install_coder_app --prefix /usr/local/bin --version VERSION app
function install_coder_app() {
    declare -a parsed_args
    local coder_app app_version
    while [ $# -gt 0 ]; do
        case "$1" in
        --prefix) parsed_args+=(--prefix "$2" --method=standalone) && shift ;;
        --version) app_version="$2" && shift ;;
        -*) log error "unknown option $1, exit!" && return 1 ;;
        *)
            if [ -z "$coder_app" ]; then
                coder_app="$1"
            else
                log error "too many arguments, please specify only one app to install!"
                return 1
            fi
            ;;
        esac
        shift
    done

    if [ -z "$coder_app" ]; then
        log error "please specify coder app to install, coder or code-server."
        return 1
    fi

    local github_proxy=$GITHUB_PROXY
    if [[ -n "$github_proxy" && ! "$github_proxy" =~ /$ ]]; then
        github_proxy="${github_proxy}/"
    fi

    local installer_url
    case $coder_app in
    coder)
        installer_url='https://coder.com/install.sh'
        if [ -n "$github_proxy" ]; then
            installer_url_fallback="${github_proxy}https://raw.githubusercontent.com/coder/coder/main/install.sh"
        fi
        app_version=$(_check_default_version --default 'stable' coder "${app_version:-${INSTALL_VERSION}}")
        if [ "$app_version" = 'stable' ]; then
            parsed_args+=(--stable)
        else
            parsed_args+=(--version "$app_version")
        fi
        ;;
    code-server)
        installer_url='https://code-server.dev/install.sh'
        if [ -n "$github_proxy" ]; then
            installer_url_fallback="${github_proxy}https://raw.githubusercontent.com/cdr/code-server/main/install.sh"
        fi
        app_version=$(_check_default_version --default 'stable' code-server "${app_version:-${INSTALL_VERSION}}")
        if [ "${app_version:?"failed to resolve ${coder_app} version to be installed!"}" != 'stable' ]; then
            parsed_args+=(--version "$app_version")
        fi
        ;;
    *) log error "unknown app ${coder_app}!" && return 1 ;;
    esac
    log debug "${coder_app} version: $app_version"

    local installer_script=''
    #! if server address resolves to ipv6, download will fail due to the network stack of
    #! container does not support IPv6.
    #! Immediate connect fail for 2606:50c0:8003::154: Cannot assign requested address
    log info "get app installer from $installer_url ......"
    if ! installer_script=$(curl -fsSL --ipv4 "$installer_url") && [ -n "$installer_url_fallback" ]; then
        log info "fallback to get app installer from $installer_url_fallback ......"
        installer_script=$(curl -fsSL --ipv4 "$installer_url_fallback")
    fi
    if [ -z "$installer_script" ]; then
        log error "fail to download app installer!" && return 1
    fi
    installer_script=$(sed -E "s|curl |curl --ipv4 |g" <<<"$installer_script")

    if [ -n "$github_proxy" ]; then
        installer_script=$(
            sed -E \
                -e "s|(https://github.com/coder/${coder_app}/releases/download)|${github_proxy}\1|g" \
                <<<"$installer_script"
        )
    fi

    log debug "parsed_args: ${parsed_args[*]}"
    if sh -s -- "${parsed_args[@]}" <<<"$installer_script"; then
        true
    else
        ERROR_CODE=$?
        if [ "$ERROR_CODE" -eq 6 ]; then
            log error "network error: please update GITHUB_PROXY ($GITHUB_PROXY)."
        fi
        return $ERROR_CODE
    fi
}

function install_terraform() {
    local _os_name _os && _get_os_name_version
    log info "install terraform on ${_os_name}($_os) ......"
    case $_os in
    debian)
        sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
        wget -O- https://apt.releases.hashicorp.com/gpg |
            gpg --dearmor |
            sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg \
                >/dev/null
        gpg --no-default-keyring \
            --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
            --fingerprint
        echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
              https://apt.releases.hashicorp.com $(lsb_release -cs) main" |
            sudo tee /etc/apt/sources.list.d/hashicorp.list
        sudo apt-get update && sudo apt-get install terraform --yes
        ;;
    *)
        log error "installation method not implemented for ${_os}!"
        return 3
        ;;
    esac
}

function install_jq() {
    local _os_name _os && _get_os_name_version
    log info "install jq on ${_os_name}($_os) ......"
    # jq is available package repositories of all following distributions.
    case $_os in
    debian)
        sudo apt-get update && sudo apt-get install jq --yes
        ;;
    rhel | fedora)
        sudo yum makecache && yum install jq --assumeyes # [-y]
        ;;
    *)
        log warn --prefix '* ' \
            "install jq for $_os via system package manager is not supported!" \
            "install latest binary release from GitHub ......"
        download_url="https://github.com/jqlang/jq/releases/latest/download/jq-linux-$(_get_mapped_container_arch)"
        local download_file=$(_download_installer "$download_url")
        _install_binary_as "$download_file" jq
        ;;
    esac
}

function install_yq() {
    local version
    INSTALL_ENABLE_DEFAULT_VERSION=false version=$(_check_default_version yq "${1:-${INSTALL_VERSION}}")
    local download_url
    # bash -e does not take effect when _check_default_version returns with non-zero
    if [ "${version:?'failed to resolve yq version to be installed!'}" = 'latest' ]; then
        log warn "install latest version of yq."
        download_url="https://github.com/mikefarah/yq/releases/latest/download/yq_linux_$(_get_mapped_container_arch)"
    else
        log info "install version ${version} of yq."
        download_url="https://github.com/mikefarah/yq/releases/download/v${version}/yq_linux_$(_get_mapped_container_arch)"
    fi
    local download_file=$(_download_installer "$download_url")
    local install_path
    _install_binary_as "$download_file" yq
}

function install_regclient() {
    # see release at: "https://github.com/regclient/regclient/releases"
    local version
    version=$(_check_default_version --default '0.7.2' regclient "${1:-${INSTALL_VERSION}}")
    local arch=$(_get_mapped_container_arch)
    declare -a components=(
        regbot
        regctl
        regsync
    )
    local c
    for c in "${components[@]}"; do
        local download_url="https://github.com/regclient/regclient/releases/download/v${version:?'failed to resolve regclient version to be installed!'}/${c}-linux-${arch}"
        local download_file=$(_download_installer "$download_url")
        local install_path
        if install_path=$(_install_binary_as "$download_file" "$c"); then
            "$install_path" version
        else
            return 1
        fi
    done
}

# INSTALL_VERSION must be specified when building container, otherwise we may
# result in installing unexpected default version.
function install_nodejs() {
    local NODE_VERSION node_version node_major version_list
    NODE_VERSION=$(_check_default_version --default 22 --hint "18.20.1 or just 18" nodejs "${1:${INSTALL_VERSION}}")

    if [[ -z "$OS_TYPE" || -z "$OS_VERSION" ]]; then
        log warning "OS_TYPE/OS_VERSION is not specified, installation may result in being incompatible with OS!"
    fi

    node_major=$(sed -En 's/^([0-9]+)(\..*)?/\1/p' <<<"${NODE_VERSION:?'failed to resolve node version to be installed!'}")
    if [ -z "$node_major" ]; then
        log error "cannot resolve major version from specified version '$NODE_VERSION'!"
        return 1
    fi
    if [[ "$OS_TYPE" = 'rhel' && "${OS_VERSION}" =~ ^7(\..*)?$ && "${node_major}" -gt 17 ]]; then
        local index_url='https://unofficial-builds.nodejs.org/download/release'
        local grep_file_tag='glibc-217'
    else
        local index_url='https://nodejs.org/dist'
    fi

    trap on_interrupt_download SIGINT
    _installer_wget_document="${HOME}/.cache/index.tab"
    if [[ "$NODE_VERSION" =~ ^[0-9]+$ ]]; then
        # using grep and awk to query index.tab in space-delimited CSV format.
        # or reading index.json with jq to filter targeted version.
        # awk '$10 !~ /-/ {print $1,$10}' for LTS release only
        # grep_file_tag is set to filter supported version on glibc 2.17
        if wget "${index_url}/index.tab" \
            --directory-prefix="${HOME}/.cache" --timestamping --quiet 1>&2; then
            true
        else
            ERROR_CODE=$?
            rm -f "$_installer_wget_document"
            return $ERROR_CODE
        fi
        version_list=$(
            grep -E "^v${NODE_VERSION:?}.*${grep_file_tag}" "$_installer_wget_document" |
                awk '$10 !~ /-/ {print $1}' | sed 's/v//'
        )
        node_version=$(head -n1 <<<"${version_list}")
        if [ -z "$node_version" ]; then
            log error "cannot find LTS release for major version ${NODE_VERSION}!" 1>&2
            return 1
        fi
    else
        node_version=$NODE_VERSION
    fi

    local _os_name _os && _get_os_name_version
    log info "install nodejs on ${_os_name}($_os) ......"
    local install_method=${INSTALL_METHOD:-$_os} # mainly for container build
    case $install_method in
    debian)
        # debian/ubuntu based
        if ! command -v node >/dev/null; then
            curl -fsSL "https://deb.nodesource.com/setup_${node_major}.x" | sudo -E bash -
        fi
        sudo apt-get install -y --no-install-recommends nodejs # will try upgrade if installed
        ;;
    rhel)
        # RHEL9 based
        if command -v node >/dev/null; then
            sudo yum update -y nodejs
        else
            curl -fsSL "https://rpm.nodesource.com/setup_${node_major}.x" | bash -
            sudo yum install -y nodejs
        fi
        ;;
    *)
        # Other Linux
        log info "using customized installation method with binary release."
        if command -v node >/dev/null && [ "$(node --version)" = "v${node_version}" ]; then
            log info "node (v$node_version) exist in the system, skip installation!"
            return 0
        fi

        _arch=$(_get_mapped_app_arch nodejs)
        if [[ ! "$_arch" =~ x64|arm64 ]]; then
            echo "error: not supported platform ${_arch}"
            return 1
        fi
        local download_file bin_search_path install_prefix
        if [[ ${OS_VERSION} = 7 && ${node_major} -gt 17 ]]; then
            if [ "$_arch" != 'x64' ]; then
                echo "error: node ${node_version} does not support ${_arch} on RHEL 7!"
                exit 1
            fi
            download_url="${index_url}/v${node_version}/node-v${node_version}-linux-${_arch}-glibc-217.tar.xz"
        else
            download_url="${index_url}/v${node_version}/node-v${node_version}-linux-${_arch}.tar.xz"
        fi
        download_file=$(_download_installer "$download_url" --continue)
        bin_search_path=$(_install_archive_as --prefix "/opt" "$download_file" node)

        # only node being added to search path, we still need to add the
        # installation directory to search path for easy access other nodejs
        # tools. e.g.,
        #+  node-v18.20.5-linux-x64/bin/node -> node-v18.20.5-linux-x64
        install_prefix=$(dirname "$(dirname "$(realpath "$bin_search_path")")")
        ln -svf --no-dereference "$install_prefix" "/opt/node-v${NODE_VERSION}" 1>&2

        if command -v node >/dev/null && [ "$(node --version)" = "v${node_version}" ]; then
            log info "node v$node_version has been installed!"
            return 0
        fi

        touch ~/.bashrc
        if grep -E 'export PATH=.*/opt/node-v.*/bin' "${HOME}/.bashrc" >/dev/null; then
            log info "existing PATH specification found, update it."
            grep -E 'export PATH=.*/opt/node-v.*/bin' "${HOME}/.bashrc"
            # NOTE: old version does not uninstalled.
            sed --in-place -E "s|(/opt/node-v.*/bin)|/opt/node-v${NODE_VERSION}/bin|" ~/.bashrc
        else
            {
                echo ""
                echo "# add node to PATH"
                echo "export PATH=\"/opt/node-v${NODE_VERSION}/bin:\$PATH\""
            } >>~/.bashrc
        fi
        # shellcheck disable=SC1090
        # we update this file
        source ~/.bashrc
        node --version
        npm --version
        log "please re-login into the shell or \`source ~/.bashrc\` to make PATH update take effect."
        ;;
    esac
}

function install_shfmt() {
    local shfmt_version
    shfmt_version=$(_check_default_version --default '3.8.0' shfmt "${1:-${INSTALL_VERSION}}")
    local _arch
    _arch=$(_get_mapped_container_arch)
    if [[ ! "$_arch" =~ amd64|arm64 ]]; then
        echo "error: not supported platform ${_arch}."
        return 1
    fi
    download_prefix="https://github.com/mvdan/sh/releases/download/v${shfmt_version:?'failed to resolve shfmt version to be installed!'}/shfmt_v${shfmt_version}_linux_${_arch}"
    local download_file=$(_download_installer "$download_prefix")
    _install_binary_as "$download_file" shfmt
}

function install_shellcheck() {
    local shellcheck_version
    shellcheck_version=$(_check_default_version --default '0.10.0' shellcheck "${1:-${INSTALL_VERSION}}")
    local _arch=$(arch)
    if [[ ! "$_arch" =~ x86_64|aarch64 ]]; then
        echo "error: not supported platform ${_arch}."
        return 1
    fi
    download_prefix="https://github.com/koalaman/shellcheck/releases/download/v${shellcheck_version:?'failed to resolve shellcheck version to be installed!'}/shellcheck-v${shellcheck_version}.linux.${_arch}.tar.xz"
    local download_file=$(_download_installer "$download_prefix")

    local install_path=$(_install_archive_as "$download_file" shellcheck)
    if [ -e "$install_path" ]; then
        "$install_path" --version
    fi
}

function install_bats() {
    local _os_name _os && _get_os_name_version
    log info "install bats on ${_os_name}($_os) ......"
    if command -v npm; then
        sudo npm install -g bats
    else
        log error --prefix '>>:' \
            "npm is required to install bats, please install nodejs at first." \
            "./bin/shutils/installer.sh nodejs"
        return 1
    fi
}

function install_nushell() {
    local nu_version
    if command -v brew >/dev/null; then
        # usually we wish to keep it up to date with brew, so do not set the version.
        if command -v nu >/dev/null; then
            log info --prefix '| ' \
                "nu ($(command -v nu), $(nu --version)) has been installed." \
                "try \`brew update && brew upgrade nushell[@x.y.z]\` to upgrade or reinstall a version."
            return 0
        fi
        nu_version=${1:-${INSTALL_VERSION:-''}}
        if [ -n "$nu_version" ]; then
            brew install "nushell@$nu_version"
        else
            brew install nushell
        fi
        log info "nu ($(nu --version)) is installed."
    else
        # install binary release from github
        nu_version=$(_check_default_version --default '0.100.0' nushell "${1:-${INSTALL_VERSION}}")
        if command -v nu >/dev/null; then
            local _nu_version=$(nu --version)
            if vcmp "${nu_version:?'failed to resolve nushell version to be installed!'}" -eq "$_nu_version"; then
                log info "nu ($(command -v nu), $nu_version) has been installed, skip!"
                return 0
            else
                log info "replace nu ($_nu_version) with version ${nu_version}."
            fi
        else
            log info "install nu of version ${nu_version}."
        fi
        local _os=linux     # {apple-darwin,pc-windows,unknown-linux}
        local _libc=gnu     # {msvc,gnu,musl}
        local _arch=$(arch) # {aarch64,x86_64}
        case $_os in
        linux) _os='unknown-linux' ;;
        esac
        local _release_url="https://github.com/nushell/nushell/releases/download/${nu_version}/nu-${nu_version}-${_arch}-${_os}-${_libc}.tar.gz"
        local download_file=$(_download_installer "$_release_url")
        local install_path=$(_install_archive_as "$download_file" nu)
        $install_path --version
    fi
}

# INSTALL_VERSION must be specified for conda when building container,
# otherwise we may result in installing unexpected default version.
function install_miniforge() {
    local _arch=$(arch)
    if [[ ! "$_arch" =~ x86_64|aarch64 ]]; then
        log error "not supported platform ${_arch}."
        return 1
    fi

    local conda_version
    conda_version=$(_check_default_version --hint "24.9.2-0 or latest" miniforge "${1:-${INSTALL_VERSION}}")
    if [[ "${conda_version:?'failed to resolve conda version to be installed!'}" = 'latest' ]]; then
        local download_url="https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-${_arch}.sh"
    else
        local download_url="https://github.com/conda-forge/miniforge/releases/download/${conda_version}/Miniforge3-${conda_version}-Linux-${_arch}.sh"
    fi
    local download_file
    download_file=$(_download_installer "$download_url")

    if [ -e /opt/miniconda3 ]; then
        extra_opts=(-u)
    fi
    if bash "${download_file}" -b -p /opt/miniconda3 "${extra_opts[@]}"; then
        true
    else
        # might fail due to "md5sum mismatch of tar archive"
        status=$?
        log error "installation failed ($status), remove the installer ${download_file}."
        rm -vf "${download_file}"
        return $status
    fi 1>&2
}

function install_caddy() {
    local caddy_version
    caddy_version=$(_check_default_version --default '2.8.4' caddy "${1:-${INSTALL_VERSION}}")
    local _arch
    _arch=$(_get_mapped_container_arch)
    if [[ ! "$_arch" =~ amd64|arm64 ]]; then
        echo "error: not supported platform ${_arch}."
        return 1
    fi

    download_prefix="https://github.com/caddyserver/caddy/releases/download/v${caddy_version:?'failed to resolve caddy version to be installed!'}/caddy_${caddy_version}_linux_${_arch}.tar.gz"
    local download_file=$(_download_installer "$download_prefix")

    local install_path=$(_install_archive_as "$download_file" caddy)
    caddy --version 1>&2
}

# install lazydocker
# brew installer will get lazydocker from github, which might be slow.
# the binary release installer can optionally work with GITHUB_PROXY to accelerate the download.
function install_lazydocker() {
    local _os_name _os && _get_os_name_version
    log info "install lazydocker on ${_os_name}($_os) ......"
    if command -v brew >/dev/null; then
        # usually we wish to keep it up to date with brew, so do not set the version.
        if command -v lazydocker >/dev/null; then
            log info --prefix '| ' \
                "lazydocker ($(command -v lazydocker), $(lazydocker --version)) has been installed." \
                "try \`brew update && brew upgrade jesseduffield/lazydocker/lazydocker\` to upgrade or reinstall a version."
            return 0
        fi
        brew install jesseduffield/lazydocker/lazydocker
        log info "lazydocker ($(lazydocker --version)) is installed."
    else
        # install binary release from github
        local lazydocker_version
        lazydocker_version=$(_check_default_version --default '0.24.1' lazydocker "${1:-${INSTALL_VERSION}}")
        local _arch
        _arch=$(_get_mapped_app_arch lazydocker)
        if [[ ! "$_arch" =~ x86_64|arm64 ]]; then
            echo "error: not supported platform ${_arch}."
            return 1
        fi
        download_prefix="https://github.com/jesseduffield/lazydocker/releases/download/v${lazydocker_version:?'failed to resolve lazydocker version to be installed!'}/lazydocker_${lazydocker_version}_Linux_${_arch}.tar.gz"
        local download_file=$(_download_installer "$download_prefix")

        local install_path=$(_install_archive_as "$download_file" lazydocker)
        if [ -e "$install_path" ]; then
            "$install_path" --version
        fi
    fi
}

if [[ "$0" =~ '-' ]]; then
    PROGNAME='bash'
else
    PROGNAME=$(basename "$0")
fi
if [ "$PROGNAME" = 'installer.sh' ]; then
    app=$1 && shift
    case $app in
    bats) install_bats ;;
    coder) install_coder_app coder "${@}" ;;
    terraform) install_terraform ;;
    jq) install_jq ;;
    yq) install_yq "${@}" ;;
    nodejs) install_nodejs "${@}" ;;
    regclient) install_regclient "${@}" ;;
    shellcheck) install_shellcheck "${@}" ;;
    nu) install_nushell "${@}" ;;
    esac
fi
