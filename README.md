# shutils

## Testing

Using `bats-core` for shell script testing.

### Creating Test Script Files

A typical test file that contains one or more test cases will be like

```bash
#!/usr/bin/env -S bats --timing
function setup() {
    source ./logging.sh  # loading functions to be tested
}
# bats test_tags=tag1, tag2
@test "addition using bc" {
  result="$(echo 2+2 | bc)"
  [ "$result" -eq 4 ]
}

@test "test with run" {
  run some_function  # run command will always succeed (return 0)
  grep 'SUCCESS' <<<"$output"
}
```

#### Labels for Test Cases

Two kinds of test case labels can be defined:

- test case scope tags: only effective to the next one test case.

  ```shell
  # bats test_tags=tag:subtag:1, tag:2, tag:3
  @test "first test" {
    # ...
  }
  ```

- file scope tags: applies to all following test cases, and repeatedly specify 
  `file_tags` will replace former definitions.

  ```shell
  # bats file_tags=tag1,tag2
  ```

Validate symbols for test case labels including alphabet, number, `_`, `-`, and `:`.
`:` usually serves as separator of sub-tags.
Multiple tags are separated by `,`.
Labels staring with `bats:` are reserved for `bats` internal use.

### Running Tests

There are two methods to run tests:

1. invoking `bats` with testing script file.

   ```shell
   bats --timing tests/test_logging.bats
   ```

   which is more flexible if you need customization to the testing procedure,
   such as [filtering test cases](#filter-running-tests).

2. adding shebang in the first line of testing script files, usually like this

   ```shell
   #!/usr/bin/env -S bats --timing --show-output-of-passing-tests
   ```

#### Filter Running Tests

By default, all test cases in a test script file will be executed.
To only running interest test cases, add some filters to the `bats`
command.
Common-used filters including:

- `-f,--filter <regex>`: only running test cases with matched test case names.
- `--filter-tags tag1,tag2,...`: only running test cases that have all of
  [labels](#labels-for-test-cases) `tag1`, `tag2`, ....
  This type filter can be specified multiple times, each option independently
  filter out a group of tests. For example, the following filters

  ```shell
  --filter-tags 'A,B' --filter-tags 'A,!C'
  ```

  will allow test cases that have tags like `(A && B) || (A && !C)`.

- `--filter-status <status>`: only running test cases with the specified last running
  status (e.g., `failed`, `missed`).
  ==This is useful, if we only want to test those failed test cases==.
