#!/bin/bash

function load() {
    # local _SCRIPTDIR_ to avoid pollute caller environment.
    local _SCRIPTDIR_
    _SCRIPTDIR_=$(dirname "$(realpath "${BASH_SOURCE[0]}")")
    if ! command -v log >/dev/null; then
        source "${_SCRIPTDIR_}/logging.sh"
    fi
}
load

function check_hosts() {
    local domain service count message
    if [ -z "$1" ]; then
        log info 'please specify a domain name to check, e.g., example.com.'
        return 0
    fi
    domain=${1}
    service=${2:-'your service'}
    if grep -E '([0-9]{1,3}\.){3}[0-9]{1,3}' <<<"$domain" >/dev/null; then
        log info "using IP address ${domain} to access ${service}."
        return 0
    fi
    #! if not match, grep return 1 trigger script exit
    # capture the error status via if test condition.
    if ! count=$(grep -cE "\\s*[^#].+\\s+${domain}" /etc/hosts); then
        count=0
    fi
    if [ "$count" -eq 0 ]; then
        log warning "no record match ${domain} found in [/etc/hosts]."
        log info "trying to resolve this domain from your DNS server ......"
        local x
        if x=$(nslookup "$domain"); then true; fi  #* prevent from exit.
        echo "$x"
    elif [ "$count" -gt 1 ]; then
        message=$(grep -E "\s+${domain}" /etc/hosts | xargs -l1 -d '\n' echo '>> ')
        log warning --prefix ' | ' \
            "duplicated IP address mapping detected in [/etc/hosts]." \
            "please remove the outdated entry to ensure unique domain-IP mapping." \
            "detail of duplicated entries:" \
            "$message"
    else
        log info --prefix ' | ' \
            "unique mapping is found:" \
            "$(grep -E "\s+${domain}" /etc/hosts | xargs -n1 -d '\n' echo '>>')"
    fi
}