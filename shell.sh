#!/bin/bash

function on_error() {
    declare -g ERROR_CODE=$?
    # shellcheck disable=SC2034
    declare -g ERROR_LINENO=$LINENO
}

trap on_error ERR

function bash_version() {
    local _version
    _version=$(bash --version | sed -En 's/.*([0-9]+\.[0-9]+\.[0-9]+).*/\1/p' | {
        head -n1
        tail >/dev/null
    })
    if [ -z "$_version" ]; then
        echo -e "error: cannot find bash version, version info of bash:\n$(bash --version)" 1>&2
        return 1
    else
        echo "$_version"
    fi
}

function version_compare() {
    # return value:
    # 1  - left > right
    # 0  - left = right
    # -1 - left < right
    local v1 v2 READ_V1 READ_V2 len
    if [ $# -lt 2 ]; then
        echo "error(${BASH_SOURCE[0]}: line $LINENO): missing compare argument."
        echo "    expected 2, get $#."
        echo "    invoking syntax: version_compare v1 v2."
        echo "    (quoted the argument, if it is empty.)"
        exit 2
    else
        v1=$1 && v2=$2
    fi
    IFS='.' read -r -a READ_V1 <<<"$v1"
    IFS='.' read -r -a READ_V2 <<<"$v2"
    local len1=${#READ_V1[@]}
    local len2=${#READ_V2[@]}
    if [ "$len1" -gt "$len2" ]; then
        len=$len1
    else
        len=$len2
    fi
    local res=0 i l r
    for ((i = 0; i < len; i++)); do
        l=${READ_V1[$i]:-0}
        r=${READ_V2[$i]:-0}
        if [ "$l" -gt "$r" ]; then
            res=1
            break
        elif [ "$l" -lt "$r" ]; then
            res='-1'
            break
        fi
    done
    echo $res
}

function vcmp() {
    # e.g., vcmp v1 -gt v2 or vcmp v1 '>' v2
    #! syntax error leads to script exit, instead of return an non-zero value
    #! if return non-zero value in such case, we cannot use ! to negate the
    #! return value.
    #* empty string '' can be passed into function as a parameter, and we can
    #* tolerate empty strings as compare operands.
    local v1 op v2 res
    if [ $# -lt 3 ]; then
        echo "error(${BASH_SOURCE[0]}: line $LINENO): missing compare argument."
        echo "    expected 3, get $#."
        echo "    invoking syntax: vcmp v1 -{gt|lt|eq|...} v2."
        echo "    (quoted the version argument, if it is empty.)"
        exit 2
    else
        v1=${1} && op=${2} && v2=${3}
    fi
    if [[ "$op" =~ (>|<|=|!|~)=? ]]; then
        op=${operators[$op]}
    fi
    if [[ ! "$op" =~ ^-(gt|ge|le|lt|eq|ne)$ ]]; then
        echo "error(${BASH_SOURCE[0]}:$LINENO): invalid compare operator [${op}]." 1>&2
        exit 2
    fi

    res=$(version_compare "$v1" "$v2")
    if eval "[ $res $op 0 ]"; then
        return 0
    else
        return 1
    fi
}

function map_operator() {
    case "$1" in
    '>') printf -- '-gt\n' ;;
    '>=') printf -- '-ge\n' ;;
    '=' | '==') printf -- '-eq\n' ;;
    '<=') printf -- '-le\n' ;;
    '<') printf -- '-lt\n' ;;
    '!=' | '~=') printf -- '-ne\n' ;; # echo '-ne' treat '-ne' as echo's options
    *)
        echo "error: invalid operator '$1'!"
        exit 1
        ;;
    esac
}

#* declare the variable to be global with -g options, so that it can be
#* referred from bats test code when loaded at setup().
#* see: <https://github.com/bats-core/bats-core/issues/922>.
#! bash v4.2.46, does not support declare dict to be reserved out of function scope.
#! bash v5.0.3, do support this feature.
#! using mapping function (map_operator) is more robust than global dict mapping.
declare -Ag operators=(
    ['>']='-gt'
    ['>=']='-ge'
    ['=']='-eq'
    ['==']='-eq'
    ['<=']='-le'
    ['<']='-lt'
    ['!=']='-ne'
    ['~=']='-ne'
)

# search an element in an array
#
# usage: arrayfind ARRAY VALUE
#
#   determine if VALUE in ARRAY.
#
# TODO add a new function array_index() and deprecate the use of arrayfind
#
# TODO define a new function search_array() to query only if the element exists.
#* In this function, using grep is sufficient for determine existence.
#+   $(IFS=$'\n'; [ grep <<<"${array[*]}" ])
function arrayfind() {
    if vcmp "$(bash_version)" -ge '4.3' ; then
        declare -n array=$1
    else
        declare -a array
        array_copy "$1" array
    fi
    local element=$2

    local i
    for ((i = 0; i < ${#array[@]}; i++)); do
        # echo "debug: $i -> ${array[i]}"
        if [ "${array[i]}" = "$element" ]; then
            echo "$i"
            return
        fi
    done
    echo '-1'
}

# subset comparison of two list (array)
# determine if array A is a subset of array B.
function subset_of_array() {
    # shellcheck disable=SC2034
    # B is referred by name in `arrayfind`
    if vcmp "$(bash_version)" -ge '4.3' ; then
        declare -n A=${1:?} B=${2:?}
    else
        declare -a A B
        array_copy "${1:?}" A
        array_copy "${2:?}" B
    fi

    local x tf_subset=0
    for x in "${A[@]}"; do
        if [ "$(arrayfind B "$x")" -lt 0 ]; then
            # key in A but not in B, so A is not a subset of B.
            # echo "debug: '$x' in $1 is not in $2." 1>&2
            tf_subset=1
            break
        fi
    done
    return $tf_subset
}

function print_dict() {
    # declare -p print as "declare -A var=([dict]="key")"
    # we only need the assigned expression.
    declare -p "$1" | sed -E 's/.*\((.*)\s*\).*/(\1)/'
}

function dict_copy() {
    source_array_name=$1
    dest_array_name=$2
    eval "$dest_array_name=$(print_dict "$source_array_name")"
    # explain eval "$dest_array_name=$(print_dict "$source_array_name")"
}

# the same as dict_copy
function array_copy(){
    source_array_name=${1:?}
    dest_array_name=${2:?}
    eval "$dest_array_name=$(print_dict "$source_array_name")"
}

# echo the actual command that is running with variables have been evaluated.
#! if command fails and the current shell is with -e option, the script
#! will immediately exit
function explain() {
    set -x
    "${@}"
    { set +x; } 2>/dev/null
    return "${ERROR_CODE:-0}"
}

# format timestamp (seconds) to human readable form.
#
# use "date +%s" to get timestamp of current time
#+  timestamp_format $(date +%s)
# use "stat --format %X %Y %Z filename" to get file time attributes
#+  timestamp_format $(stat --format '%X' ~/.bashrc)
function timestamp_format() {
    local ts=${1:?}
    local format=${2:-'%Y-%m-%d %H:%M:%S'}

    date --date "@${ts}" +"$format"
}
